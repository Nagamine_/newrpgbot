import { ButtonInteraction, Collection, StringSelectMenuInteraction } from "discord.js"
import Skill from "../game/skills/Skill"
import SkillScript from "../game/skillScripts/SkillScripts"
import Archetipe from "../game/character/Archetipe"
import { Command, InteractionCommand } from "../discord/Command"
import Location from "../game/world/Location"
import Building from "../game/world/Building"
import BuildingTemplate from "../game/world/BuildingTemplate"

let commands = new Collection<string,Command>()
let interactionCommands = new Collection<string,InteractionCommand<ButtonInteraction | StringSelectMenuInteraction>>()
let skills = new Collection<string, Skill>()
let skillScripts= new Collection<string, SkillScript>()
let archetipes = new Collection<string, Archetipe>()
let locations = new Collection<string, Location>()
let buildings = new Collection<string,BuildingTemplate>()
export default {
    commands,
    interactionCommands,
    skills,
    skillScripts,
    archetipes,
    locations,
    buildings
}