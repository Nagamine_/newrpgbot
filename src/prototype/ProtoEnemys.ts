import BattleEntity, { IABattleEntity } from "../game/battle/BattleEntity";
import BattleEntityUtils from "../game/battle/BattleEntityUtils";
import { Stats } from "../game/stats/Stats";
import GlobalContext from "../globalContext/GlobalContext";
import GameUtils from "../utils/GameUtils";

const createEnemy = (name: string,battleId: string, stats?:Stats) : BattleEntity => {
   return BattleEntityUtils.create({
        name: name,
        id: GameUtils.generateUniqueID(),
        controller: 'IA',
        battleId: battleId,
        bando: "2",
        stats: stats ?? {
            vida: 220,
            fuerza: 5,
            velocidad: 1,
            defensa: 1
        },
        skills: [GlobalContext.skills.get("cuchillazo")!]
    })
}
export default {
    createEnemy
}