function replacer(key: any, value: any) {
    if (value instanceof Map) {
        return {
            dataType: 'Map',
            value: Array.from(value.entries()), // or with spread: value: [...value]
        }
    } else {
        return value
    }
}

function reviver(key: any, value: any) {
    if (typeof value === 'object' && value !== null) {
        if (value.dataType === 'Map') {
            return new Map(value.value)
        }
    }
    return value
}

function parse(str: string) {
    return JSON.parse(str, reviver)
}

function stringify(object: any) {
    return JSON.stringify(object, replacer)
}

export default {
    parse,
    stringify,
}
