//use central limit theorem to generate a normal distribution
function gaussianRand() {
    var rand = 0;
    for (var i = 0; i < 6; i += 1) {
        rand += Math.random();
    }
    return rand / 6;
}
function gaussianRandom(mean:number, stdev:number) {
    // const randomSign = Math.random() < 0.5 ? -1 : 1;
  return  gaussianRand()*2 *stdev /*randomSign*/+mean;
}
//get random number between range
function getRandomFloat(min:number, max:number) {
    return Math.random() * (max - min) + min;
}
function getRandomInt(min:number, max:number) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export default {
    gaussianRand,
    gaussianRandom,
    getRandomFloat,
    getRandomInt
}
