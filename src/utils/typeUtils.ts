//Retorna un tipo con los atributos de K como reqeridos
export type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] }
/**https://stackoverflow.com/questions/69327990/how-can-i-make-one-property-non-optional-in-a-typescript-type
 * type User = {
  id: string
  name?: string
  email?: string
}

type WithRequired<T, K extends keyof T> = T & { [P in K]-?: T[P] }

type UserWithName = WithRequired<User, 'name'>

// error: missing name
const user: UserWithName = {
  id: '12345',
}
 */