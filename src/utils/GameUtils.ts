import MathUtils from "./MathUtils";

function generateUniqueID(){
    return  Date.now().valueOf().toString(36) + Math.random().toString(36).slice(2) 
}
function getRandomItem<T> (list: Array<T>) {
    return list[Math.floor((Math.random()*list.length))];
  }
  //take the percent of the base number and return a random percentage between 0 and the base deviation
  //ejem  GameUtils.radomizeReward(oro,20,5) =  20% de oro +- 5% de oro distribucion normal
 function radomizeReward(baseNumber:number, basePercent:number,baseDeviation:number){
    return Math.ceil(MathUtils.gaussianRandom((baseNumber * basePercent/100), baseNumber*baseDeviation/100))
  }

export default{
    generateUniqueID,
    getRandomItem,
    radomizeReward
}
