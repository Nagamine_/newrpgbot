import { ButtonInteraction, Interaction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'
import BattleInteractionManager from '../../../game/battle/BattleInteractionManager'
import BattleEntity from '../../../game/battle/BattleEntity'
import BattleBD from '../../../repository/BattleBD'
import { characterToBattleEntity } from '../../../game/character/CharacterUtils'
import { WithRequired } from '../../../utils/typeUtils'
const execute = async (interaction: ButtonInteraction, ctx: WithRequired<CommandContext, "character" | "characterData">) => {
    const character = ctx.character
    const data = ctx.characterData
    const battle = await BattleBD.read(ctx.args[0])
    if (battle === undefined) {
        interaction.reply({
            content: `Error, no hay batalla`,
            components: [],
        })
        return
    }
    const entity: BattleEntity = characterToBattleEntity(character,data, battle.id)
    BattleInteractionManager.joinBattle({
        battle,
        interaction,
        battleEntity: entity
    })
}
const battleTest: InteractionCommand<ButtonInteraction> = {
    name: 'joinBattle',
    description: 'joinBattle',
    execute: execute,
    permisions: {
        character :{
            basicInfo: true,
            data: true,
            battle: false,
        },
        argNumber: 1
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
