import { ButtonInteraction, Interaction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'
import globalContext from '../../../globalContext/GlobalContext'
import BattleInteractionManager from '../../../game/battle/BattleInteractionManager'
const execute = async (interaction: ButtonInteraction, ctx: Required<CommandContext>) => {
    const battle = ctx.battle
    const userBattleEntity = battle.getUserEntity(interaction.user.id)
    if(userBattleEntity === null){
        interaction.reply({
            content: `Error, no hay batalla`
        })
        return
    }
    const attackKey = ctx.args[0]
    const skill = globalContext.skills.get(attackKey) 
    if (skill === undefined) {
        console.error("ERROR - skillButton.ts - ataque no existente")
        return
    }
    // await interaction.update({
    //     content: "Replied",
    //     components: []
    // })
    BattleInteractionManager.skillSelect({
        battle,
        interaction,
        battleEntity: userBattleEntity,
        skill
    })
}
const battleTest: InteractionCommand<ButtonInteraction> = {
    name: 'skill',
    description: 'skill',
    execute: execute,
    permisions: {
        character :{
            battle: true,
        },
        argNumber: 1
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
