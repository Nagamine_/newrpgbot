import { ButtonInteraction, Interaction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'

import BattleUtils from '../../../game/battle/BattleUtils'
import BattleInteractionManager from '../../../game/battle/BattleInteractionManager'
import { WithRequired } from '../../../utils/typeUtils'
const execute = async (interaction: ButtonInteraction, ctx: WithRequired<CommandContext, "battle">) => {
    const battle = ctx.battle
    const userBattleEntity = battle.getUserEntity(interaction.user.id)
    if (userBattleEntity === null) {
        interaction.reply({
            content: `Error, no hay batalla`,
        })
        return
    }
    const objetiveKey = ctx.args[0]
    const objetive = BattleUtils.getBattleEntityById(
        battle.entities,
        objetiveKey
    )
    if (objetive === null) {
        console.error('ERROR - objetivo no existente')
        return
    }
    await interaction.update({
        content: 'Esperando al resto de los jugadores',
        components: [],
    })
    BattleInteractionManager.objetiveSelect({
        battle,
        interaction,
        battleEntity: userBattleEntity,
        objetive
    })
}
const objetivesButton: InteractionCommand<ButtonInteraction> = {
    name: 'objectiveButton',
    description: 'objectiveButton',
    execute: execute,
    permisions: {
        character :{
            battle: true,
        },
        argNumber: 1
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default objetivesButton
