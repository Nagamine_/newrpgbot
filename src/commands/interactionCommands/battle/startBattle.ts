import { ButtonInteraction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import BattleInteractionManager from '../../../game/battle/BattleInteractionManager'
import { InteractionCommand } from '../../../discord/Command'

//TODO rename startBattle
const execute = (interaction: ButtonInteraction, ctx: Required<CommandContext>) => {
    const battle = ctx.battle
    BattleInteractionManager.startBattle({
        battle,
        interaction,
    })
}
const startBattle: InteractionCommand<ButtonInteraction> = {
    name: 'startBattle',
    description: 'startBattle',
    execute: execute,
    permisions: {
        character: {
            battle: true,
        },
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default startBattle
