import { Interaction, StringSelectMenuInteraction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'

import BattleUtils from '../../../game/battle/BattleUtils'
import BattleInteractionManager from '../../../game/battle/BattleInteractionManager'
import { WithRequired } from '../../../utils/typeUtils'
const execute = async (interaction: StringSelectMenuInteraction, ctx: WithRequired<CommandContext, "battle">) => {
    const battle = ctx.battle
    const userBattleEntity = battle.getUserEntity(interaction.user.id)
    if (userBattleEntity === null) {
        interaction.reply({
            content: `Error, no hay batalla`,
        })
        return
    }
    const objetiveKey = interaction.values[0]
    const objetive = BattleUtils.getBattleEntityById(
        battle.entities,
        objetiveKey
    )
    if (objetive === null) {
        console.error('ERROR - objetivo no existente')
        return
    }
    await interaction.update({
        components: [],
    })
    BattleInteractionManager.objetiveSelect({
        battle,
        interaction,
        battleEntity: userBattleEntity,
        objetive
    })
}
const battleTest: InteractionCommand<StringSelectMenuInteraction> = {
    name: 'objectiveSelect',
    description: 'objectiveSelect',
    execute: execute,
    permisions: {
        character :{
            battle: true,
        },
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
