import { StringSelectMenuInteraction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'
import { SelectProfileDefault } from '../../../discord/Interactions/characterInteractions/createSelectProfileDropdown'
import createCharacterSummaryEmbed from '../../../discord/Interactions/characterInteractions/createCharacterSummaryEmbed'
import createCharacterEquipmentEmbed from '../../../discord/Interactions/characterInteractions/createCharacterEquipmentEmbed'
import createCharacterSkillEmbed from '../../../discord/Interactions/characterInteractions/createCharacterSkillEmbed'
import createCharacterInventoryEmbed from '../../../discord/Interactions/characterInteractions/createCharacterInventoryEmbed'
const execute = async (interaction: StringSelectMenuInteraction, ctx: Required<CommandContext>) => {
    const key = interaction.values[0]
    const character = ctx.character
    const inventory = ctx.inventory
    const data = ctx.characterData
    switch(key){
        case SelectProfileDefault.RESUMEN:
            await interaction.update(createCharacterSummaryEmbed(character,data,inventory))
            return
        case SelectProfileDefault.EQUIPO:
            await interaction.update(createCharacterEquipmentEmbed(character))
            return
        case SelectProfileDefault.HABILIDADES:
            await interaction.update(createCharacterSkillEmbed(character))
            return
        case SelectProfileDefault.INVENTARIO:
            await interaction.update(createCharacterInventoryEmbed(inventory))
            return
        default:
            await interaction.update(createCharacterSummaryEmbed(character,data,inventory))
    }
}
const selectProfile: InteractionCommand<StringSelectMenuInteraction> = {
    name: 'selectProfile',
    description: 'selectProfile',
    execute: execute,
    permisions: {
        character :{
            basicInfo: true,
            inventory: true,
            data: true,
            battle: undefined,
        },
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default selectProfile