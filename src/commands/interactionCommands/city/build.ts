import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle, Interaction } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'
import { WithRequired } from '../../../utils/typeUtils'
import GlobalContext from '../../../globalContext/GlobalContext'
import BuildingController from '../../../game/world/BuildingController'
import { isErrorComunicator } from '../../../game/errors/ErrorComunicator'
const execute = async (interaction: ButtonInteraction, ctx: WithRequired<CommandContext, "serverGeography" | "inventory">) => {
    const geography = ctx.serverGeography
    const inventory = ctx.inventory

    if(ctx.args[0] === undefined) {
        console.error("Error - build.ts - No se ha seleccionado ningun edificio")
        interaction.reply({
            content: "Error - No se ha seleccionado ningun edificio",
            ephemeral: true,
        })
        return
    }
    const buildingId = ctx.args[0]
    const result = BuildingController.buildBuilding(buildingId, geography, inventory)
    if(isErrorComunicator(result)) {
        interaction.reply({
            content: result.message,
            ephemeral: true,
        })
        return
    }
    const building = result
    const embed = {
        title: "Construiste:",
        description: building.name,
        fields: [
            {
                name: building.levelDescription.name,
                value: building.levelDescription.description,
            },
        ]
    }
    return interaction.reply({
        embeds: [embed],
    })

}
const battleTest: InteractionCommand<ButtonInteraction> = {
    name: 'build',
    description: 'build',
    execute: execute,
    permisions: {
        location: true,
        character: {
            inventory: true,
        }
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
