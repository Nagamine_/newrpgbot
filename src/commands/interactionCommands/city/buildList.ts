import { ActionRowBuilder, ButtonBuilder, ButtonInteraction, ButtonStyle } from 'discord.js'
import CommandContext from '../../../discord/CommandContext'
import { InteractionCommand } from '../../../discord/Command'
import { WithRequired } from '../../../utils/typeUtils'
import { itemListToString } from '../../../game/items/ItemUtils'
import BuildingController from '../../../game/world/BuildingController'
const execute = async (interaction: ButtonInteraction, ctx: WithRequired<CommandContext, "serverGeography">) => {
    const geography = ctx.serverGeography

    //Reply with the list of buildings that can be built with their price
    let buildingFields: {
        id: string,
        name: string,
        value: string,
    }[] = []
    const buildings =  BuildingController.getNextBuildingsList(geography)
    buildingFields = buildings.map(building => {
        return {
            id: building.id,
            name: building.levelTemplate.name,
            value: building.levelTemplate.description + "\n**Precio:**\n" + itemListToString(building.levelTemplate.cost).join("\n"),
        }
    })

    //create a button per building
    const buttons: ButtonBuilder[] = []
    buildingFields.forEach(building => {
        buttons.push(
            new ButtonBuilder()
                .setCustomId("build::" + building.id)
                .setLabel(building.name)
                .setStyle(ButtonStyle.Primary)
        )
    })
    const row = new ActionRowBuilder<ButtonBuilder>().addComponents(buttons)
    //Create an embed that reflects that
    const embed = {
        title: "Construir edificios",
        description: "Edificios disponibles para construir",
        fields: buildingFields,
    }
    interaction.reply({
        embeds: [embed],
        components: [row],
    })


}
const battleTest: InteractionCommand<ButtonInteraction> = {
    name: 'buildList',
    description: 'buildList',
    execute: execute,
    permisions: {
        location: true
    },
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
