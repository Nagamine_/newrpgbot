import { Message } from "discord.js"
import CommandContext from "../../../discord/CommandContext"
import BattleEntity from "../../../game/battle/BattleEntity"
import { Command } from "../../../discord/Command"
import Battle from "../../../game/battle/Battle"
import GameUtils from "../../../utils/GameUtils"
import createStartBattleButtons from "../../../discord/Interactions/battleInteractions/CreateStartBattleButtons"
import BattleBD from "../../../repository/BattleBD"
import BattleEntityUtils from "../../../game/battle/BattleEntityUtils"
import ProtoEnemys from "../../../prototype/ProtoEnemys"
import GlobalContext from "../../../globalContext/GlobalContext"
import { CategoryList } from "../../../discord/CommandCategory"

const execute = (msg: Message, ctx: CommandContext) => {
    const battle: Battle = new Battle({
        maxParticipants: 1,
    })
    const enemy: BattleEntity = ProtoEnemys.createEnemy("rata",battle.id)
    const enemy2: BattleEntity = ProtoEnemys.createEnemy("rata 2",battle.id,{
        vida: 100,
        fuerza: 5,
        velocidad: 20,
        defensa: 1
    })
    const user: BattleEntity = BattleEntityUtils.create({
        name: "user",
        id: GameUtils.generateUniqueID(),
        battleId: battle.id,
        controller: 'HUMAN',
        user: msg.author.id,
        bando: "1",
        stats: {
            vida: 5000,
            fuerza: 7,
            velocidad: 8,
            defensa: 1
        },
        skills: [
            GlobalContext.skills.get("cuchillazo"), 
            GlobalContext.skills.get("envenenamiento"), 
        ]
    })
    battle.entities.push(enemy,enemy2,user)
    BattleBD.save(battle)
    BattleBD.addUsertoBattle(battle.id,msg.author.id)
    
    msg.reply(createStartBattleButtons(battle))
}
const battleTest: Command = {
    name: 'battleTest',
    alias: ["bt"],
    category: CategoryList.TEST,
    description: 'BattleTest',
    execute: execute,
    permisions: {
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
