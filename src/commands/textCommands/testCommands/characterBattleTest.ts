import { Message } from "discord.js"
import CommandContext from "../../../discord/CommandContext"
import BattleEntity from "../../../game/battle/BattleEntity"
import { Command } from "../../../discord/Command"
import Battle from "../../../game/battle/Battle"
import GameUtils from "../../../utils/GameUtils"
import createStartBattleButtons from "../../../discord/Interactions/battleInteractions/CreateStartBattleButtons"
import BattleBD from "../../../repository/BattleBD"
import { characterToBattleEntity } from "../../../game/character/CharacterUtils"
import BattleEntityUtils from "../../../game/battle/BattleEntityUtils"
import GlobalContext from "../../../globalContext/GlobalContext"
import { CategoryList } from "../../../discord/CommandCategory"
const execute = (msg: Message, ctx: CommandContext) => {
    const character = ctx.character!
    const data = ctx.characterData!
    const battle: Battle = new Battle({
        maxParticipants: 1,
    })
    const enemy: BattleEntity = BattleEntityUtils.create({
        name: "rata",
        id: GameUtils.generateUniqueID(),
        controller: 'IA',
        battleId: battle.id,
        bando: "2",
        stats: {
            vida: 220,
            fuerza: 5,
            velocidad: 9,
            defensa: 1
        },
        skills: [GlobalContext.skills.get("cuchillazo")]
    })
    // const enemy2: BattleEntity = new BattleEntity({
    //     name: "rata 2",
    //     id: GeneralUtils.generateUniqueID(),
    //     user: BattleEntityController.IA,
    //     bando: "2",
    //     stats: {
    //         vida: 100,
    //         fuerza: 5,
    //         velocidad: 20,
    //         defensa: 1
    //     },
    //     skills: ["cuchillazo"]
    // })
    const user: BattleEntity = characterToBattleEntity(character,data,battle.id)
    battle.entities.push(enemy,user)
    BattleBD.save(battle)
    BattleBD.addUsertoBattle(battle.id,msg.author.id)
    msg.channel.send(createStartBattleButtons(battle))
}
const characterBattleTest: Command = {
    name: 'characterBattleTest',
    alias: ["cbt"],
    category: CategoryList.TEST,
    description: 'characterBattleTest',
    execute: execute,
    permisions: {
        character: {
            basicInfo: true,
            data: true,
            battle: false
        }
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default characterBattleTest
