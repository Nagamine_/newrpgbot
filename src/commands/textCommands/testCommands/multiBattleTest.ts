import { Message } from "discord.js"
import CommandContext from "../../../discord/CommandContext"
import BattleEntity from "../../../game/battle/BattleEntity"
import { Command } from "../../../discord/Command"
import Battle from "../../../game/battle/Battle"
import createJoinBattleButtons from "../../../discord/Interactions/battleInteractions/CreateJoinBattleButtons"
import BattleBD from "../../../repository/BattleBD"
import ProtoEnemys from "../../../prototype/ProtoEnemys"
import { CategoryList } from "../../../discord/CommandCategory"
const execute = (msg: Message, ctx: CommandContext) => {
    const battle: Battle =  new Battle({
        maxParticipants: 3,
    })
    const enemy: BattleEntity = ProtoEnemys.createEnemy("rata",battle.id)
    const enemy2: BattleEntity = ProtoEnemys.createEnemy("rata 2",battle.id,{
        vida: 100,
        fuerza: 5,
        velocidad: 20,
        defensa: 1
    })
    battle.entities.push(enemy,enemy2)
    BattleBD.save(battle)
    msg.channel.send(createJoinBattleButtons(battle))
}
const battleTest: Command = {
    name: 'multiBattleTest',
    alias: ["mbt"],
    category: CategoryList.TEST,
    description: 'multiBattleTest',
    execute: execute,
    permisions: {
        character: {
            battle: false
        }
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default battleTest
