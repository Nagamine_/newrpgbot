import { Message } from "discord.js"
import CommandContext from "../../../discord/CommandContext"
import GlobalContext from "../../../globalContext/GlobalContext"
import { Command } from "../../../discord/Command"
import { CategoryList, CommandCategory } from "../../../discord/CommandCategory"

const execute = (msg: Message, ctx: CommandContext) => {
    const categoryMap = new Map<CommandCategory,Map<string, Command>>()
    GlobalContext.commands.forEach((command) => {
        if(categoryMap.has(command.category)){
            categoryMap.get(command.category)!.set(command.name, command)
        }else{
            categoryMap.set(command.category, new Map<string, Command>().set(command.name, command))
        }
    })
    const categoryList = Array.from(categoryMap.keys())
    categoryList.sort((a, b) => {
        return a.order - b.order
    })
    let comandos: string = ""
    categoryList.forEach((category) => {
        comandos += "\n**" + category.name + "** ~ " + category.description + "\n"
        categoryMap.get(category)!.forEach((it) => {
            comandos += "`" + it.name + "`"
            comandos += it.alias.length > 0 ? " ~ " + it.alias.map((alias) => { return "`" + alias + "`" }).join(" ") : ""
            comandos += " ∷ " + it.description
            comandos += "\n"
        })
    })
    const embed = {
        "title": "Ayuda",
        "description": 'El prefijo del bot es: `!`' + '\n' + comandos,
        "color": 0x00FFFF,
    }
    msg.channel.send({
        embeds: [embed]
    })
}
const commands: Command = {
    name: 'commands',
    alias: ['help', "comandos"],
    category: CategoryList.INFORMATION,
    description: 'Obtiene la lista de comandos disponibles',
    execute: execute,
    permisions: {},
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default commands
