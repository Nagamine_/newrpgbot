import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import { CategoryList } from "../../../discord/CommandCategory"

const execute = (msg: Message) => {
    const timeTaken = Date.now() - msg.createdTimestamp
    //   msg.reply(`Pong! demoro ${timeTaken}ms. este mensaje`)
    msg.channel.send({
        content: `Pong! demoro ${timeTaken}ms. este mensaje`,
    })
}
const ping: Command = {
    name: 'ping',
    alias: ["p"],
    category: CategoryList.UTILITY,
    description: 'ping pong! esta funcionando el bot?',
    execute: execute,
    permisions: {
        servidor: "ninguno"
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default ping
