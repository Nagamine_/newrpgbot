import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import BattleBD from "../../../repository/BattleBD"
import { CategoryList } from "../../../discord/CommandCategory"
import BattleUtils from "../../../game/battle/BattleUtils"

const execute = async (msg: Message, ctx: CommandContext) => {
    const battle = ctx.battle!
    BattleBD.deleteUserFromBattle(msg.author.id)
    const entity = battle.entities.find(u => u.user === msg.author.id)
    if(entity && BattleUtils.getAllyEntities(entity, battle.entities).length === 0 ){
        BattleBD.remove(battle.id)
    }
    msg.channel.send(`Huiste de la batalla!`)
}
const runBattle: Command = {
    name: 'run',
    alias: ['huir'],
    category: CategoryList.GAMEPLAY,
    description: 'Huye del combate',
    execute: execute,
    permisions: {
        character: {
            battle: true,
        }
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default runBattle
