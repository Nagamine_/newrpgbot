import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import { CategoryList } from "../../../discord/CommandCategory"
import CommandContext from "../../../discord/CommandContext"
import CharacterDataDB from "../../../repository/CharacterDataDB"

const execute = async (msg: Message, ctx: CommandContext) => {
    const data = ctx.characterData!

    if (data.statsModifier.vida < 0) {
        data.statsModifier.vida = 0
        msg.channel.send('Tu personaje ha restaurado su vida')
        CharacterDataDB.create(data)
    }
}
const heal: Command = {
    name: 'heal',
    alias: ['curar'],
    category: CategoryList.GAMEPLAY,
    description: 'Cura al personaje (cooldown 1 hora)',
    execute: execute,
    permisions: {
        character: {
            data: true,
            battle: false,
        },
        conditional: {
            fn: (ctx: CommandContext) => ctx.characterData!.statsModifier.vida < 0,
            message: 'Tu personaje tiene la vida completa',
            permisions: {
                cooldown: 3600
            }
        }
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default heal
