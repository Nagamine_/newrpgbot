import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import { CategoryList } from "../../../discord/CommandCategory"

const execute = async (msg: Message, ctx: Required<CommandContext>) => {
    const charLoc = ctx.characterLocation
    //crea un embed que muestre los datos
    const embed = {
        title: charLoc.name,
        description: charLoc.description,
        fields: [
            {
                name: 'Enemigos',
                value: charLoc.enemies.map( enemy => enemy.name ).join('\n'),
                inline: true,
            },
            {
                name: 'Materiales de exploracion',
                value: Array.from(charLoc.rewardItems.keys()).join('\n'),
                inline: true,
            },
            {
                name: 'Salario',
                value: charLoc.salary.toString(),
                inline: true,
            },
        ],
    }
    //envia el embed
    await msg.channel.send({content: "Tu ubicación actual es:", embeds: [embed]})
}
const location: Command = {
    name: 'location',
    alias: ['ubicacion','l'],
    category: CategoryList.GAMEPLAY,
    description: 'Informacion dela ubicacion actual',
    execute: execute,
    permisions: {
        character: {
            data: true,
        },
        location: true,
    }
}
export default location
