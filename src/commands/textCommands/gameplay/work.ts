import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import GameUtils from "../../../utils/GameUtils"
import InventoryDB from "../../../repository/InventoryDB"
import { CategoryList } from "../../../discord/CommandCategory"
const execute = (msg: Message,ctx: CommandContext) => {
    const inventory = ctx.inventory!
    let oro = inventory.items.get('oro') ?? 0
    let reward = GameUtils.radomizeReward(oro,20,5) //20% de oro +- 5% de oro distribucion normal
    inventory.items.set('oro',oro+reward)
    InventoryDB.create(inventory)
   msg.channel.send('Has trabajado y ganado '+reward+' de oro')
}
const work: Command = {
    name: 'work',
    alias: ['trabajar','w'],
    category: CategoryList.GAMEPLAY,
    description: 'Obtiene dinero',
    execute: execute,
    permisions: {
        character: {
            inventory: true,
            battle: false,
        },
        cooldown: 60*5
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default work
