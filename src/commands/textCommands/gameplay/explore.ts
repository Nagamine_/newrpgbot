import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import CharacterDataDB from "../../../repository/CharacterDataDB"
import GlobalContext from "../../../globalContext/GlobalContext"
import { itemListToString, calculateRewards } from "../../../game/items/ItemUtils"
import { addItemsToInventory } from "../../../game/character/Inventory"
import InventoryDB from "../../../repository/InventoryDB"
import { CategoryList } from "../../../discord/CommandCategory"
import { getCurrentLocation } from "../../../game/character/CharacterData"

const execute = async (msg: Message, ctx: CommandContext) => {
    const data = ctx.characterData!
    const inventory = ctx.inventory!
    const location = GlobalContext.locations.get(getCurrentLocation(data, msg.guildId))
    const items = calculateRewards(location?.rewardItems)
    
    InventoryDB.create(addItemsToInventory(inventory, items))
    const reply = `Has explorado ${location?.name} y has encontrado:\n>>> ${itemListToString(items).join('\n')}`
    msg.channel.send(reply)
}
const heal: Command = {
    name: 'explore',
    alias: ['explorar', 'e'],
    category: CategoryList.GAMEPLAY,
    description: 'Explora items (cooldown 5 minutos)',
    execute: execute,
    permisions: {
        character: {
            data: true,
            inventory: true,
            battle: false,
        },
        cooldown: 5
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default heal
