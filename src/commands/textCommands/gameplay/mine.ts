import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import GameUtils from "../../../utils/GameUtils"
import InventoryDB from "../../../repository/InventoryDB"
import { CategoryList } from "../../../discord/CommandCategory"
const execute = (msg: Message,ctx: CommandContext) => {
    const inventory = ctx.inventory!
    let oro = inventory.items.get('oro') ?? 0
    let reward = GameUtils.radomizeReward(oro,20,5) //20% de oro +- 5% de oro distribucion normal
    inventory.items.set('oro',oro+reward)
    InventoryDB.create(inventory)
   msg.channel.send('Has minado y ganado '+reward+' de oro')
}
const mine: Command = {
    name: 'mine',
    alias: ['minar','m'],
    category: CategoryList.GAMEPLAY,
    description: 'Obtiene dinero minando',
    execute: execute,
    permisions: {
        character: {
            inventory: true,
            battle: false,
        },
        location: {
            requiredBuildings: [{
                buildingId: 'mina'
            }]
        },
        cooldown: 60*5
    }
}
export default mine
