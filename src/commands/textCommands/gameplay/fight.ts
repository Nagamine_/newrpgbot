import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import GlobalContext from "../../../globalContext/GlobalContext"
import Battle from "../../../game/battle/Battle"
import GameUtils from "../../../utils/GameUtils"
import BattleEntityUtils from "../../../game/battle/BattleEntityUtils"
import { characterToBattleEntity } from "../../../game/character/CharacterUtils"
import BattleBD from "../../../repository/BattleBD"
import createStartBattleButtons from "../../../discord/Interactions/battleInteractions/CreateStartBattleButtons"
import { CategoryList } from "../../../discord/CommandCategory"

const execute = async (msg: Message, ctx: CommandContext) => {
    const character = ctx.character!
    const data = ctx.characterData!
    const inventory = ctx.inventory!
    const location = ctx.characterLocation!
    const battle: Battle = new Battle();
    const enemy = GameUtils.getRandomItem(location.enemies)
    const battleEntity = BattleEntityUtils.createFromPrototype(enemy, battle.id)
    const user = characterToBattleEntity(character,data,battle.id)
    battle.entities.push(battleEntity,user)
    battle.reward = enemy.items
    BattleBD.save(battle)
    BattleBD.addUsertoBattle(battle.id,msg.author.id)
    msg.channel.send(createStartBattleButtons(battle,`Te encontraste con un ${enemy.name}!`))
}
const fight: Command = {
    name: 'fight',
    alias: ['luchar', 'f'],
    category: CategoryList.GAMEPLAY,
    description: 'Batalla contra los enemigos del mapa (no cooldown)',
    execute: execute,
    permisions: {
        character: {
            basicInfo: true,
            data: {
                alive: true,
            },
            inventory: true,
            battle: false,
        },
        location: true
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default fight
