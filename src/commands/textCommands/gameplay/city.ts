import { ActionRowBuilder, ButtonBuilder, ButtonStyle, Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import BattleBD from "../../../repository/BattleBD"
import { CategoryList } from "../../../discord/CommandCategory"

const execute = async (msg: Message, ctx: Required<CommandContext>) => {
    const geography = ctx.serverGeography
    //Get an string of buildings with level
    let buildings = ""
    geography.buildings.forEach( building => {
        buildings += `${building.name} (Nivel ${building.level})\n`
    })
    //Get an string of locations names
    let locations = ""
    geography.discoveredLocations.forEach( location => {
        locations += `${location.name}\n`
    })
    //crea un embed que muestre los datos de informacion de los edificios de la ciudad y las ubicaciones disponibles
    const embed = {
        title: "Informacion de la ciudad",
        description: "Informacion de los edificios de la ciudad y las ubicaciones disponibles",
        fields: [
            {
                name: 'Edificios',
                value: buildings,
            },
            {
                name: 'Ubicaciones descubiertas',
                value: locations,
            },
        ],
    }
    //add a button that says build
    const button = new ButtonBuilder()
    .setCustomId('buildList')
    .setLabel('Construir edificios')
    .setStyle(ButtonStyle.Primary)

    const row = new ActionRowBuilder<ButtonBuilder>()
    .addComponents(
        button)

    return msg.channel.send({embeds: [embed], components: [row]})
}
const city: Command = {
    name: 'city',
    alias: ['ciudad'],
    category: CategoryList.GAMEPLAY,
    description: 'Informacion de los edificios de la ciudad y las ubicaciones disponibles',
    execute: execute,
    permisions: {
        location: true,
    }
}
export default city
