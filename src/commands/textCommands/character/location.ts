import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import CommandContext from "../../../discord/CommandContext"
import GlobalContext from "../../../globalContext/GlobalContext"
import createLocationInfoEmbed from "../../../discord/Interactions/worldInteractions/CreateLocationInfoEmbed"
import { CategoryList } from "../../../discord/CommandCategory"
import { getCurrentLocation } from "../../../game/character/CharacterData"
const execute = (msg: Message,ctx: CommandContext) => {
    const data = ctx.characterData!
    const location = GlobalContext.locations.get(getCurrentLocation(data, msg.guildId))
    if(!location) return msg.channel.send('No se ha encontrado la ubicación actual')

    msg.channel.send(createLocationInfoEmbed(location))
}
const worldInfo: Command = {
    name: 'location',
    alias: ['l','ubicacion'],
    category: CategoryList.CHARACTER,
    description: 'Obtiene la información de la ubicacion actual',
    execute: execute,
    permisions: {
        character: {
            data: true,
            battle: undefined,
        },
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default worldInfo
