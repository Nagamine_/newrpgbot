import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import createCharacterSummary from '../../../discord/Interactions/characterInteractions/createCharacterSummaryEmbed'
import CommandContext from "../../../discord/CommandContext"
import { CategoryList } from "../../../discord/CommandCategory"
const execute = (msg: Message,ctx: CommandContext) => {
    const character = ctx.character!
    const characterData= ctx.characterData!
    const inventario = ctx.inventory!
    msg.channel.send(createCharacterSummary(character, characterData,inventario))
}
const profile: Command = {
    name: 'profile',
    alias: [],
    category: CategoryList.CHARACTER,
    description: 'Muestra el perfil del personaje',
    execute: execute,
    permisions: {
        character: {
            basicInfo: true,
            inventory: true,
            data: true,
            battle: undefined,
        }
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default profile
