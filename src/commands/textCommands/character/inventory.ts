import { Message } from "discord.js"
import { Command } from "../../../discord/Command"
import { CategoryList } from "../../../discord/CommandCategory"
import CommandContext from "../../../discord/CommandContext"
import createCharacterInventoryEmbed from "../../../discord/Interactions/characterInteractions/createCharacterInventoryEmbed"

const execute = async (msg: Message, ctx: Required<CommandContext>) => {
    const inventory = ctx.inventory

    msg.channel.send(createCharacterInventoryEmbed(inventory))
}
const inventory: Command = {
    name: 'inventory',
    alias: ['inventario', 'i'],
    category: CategoryList.CHARACTER,
    description: 'Muestra el inventario del personaje',
    execute: execute,
    permisions: {
        character: {
            inventory: true,
        },
        cooldown: 5
    }
    // permisos: {
    //     registrado: false,
    //     servidor: "ninguno"
    // }
}
export default inventory