import MathUtils from "../../utils/MathUtils";
import { ItemList, ItemReward } from "./Item";
export function itemRewardToString(itemReward?: ItemReward): string {
    let result = "";
    if (itemReward === undefined) return result;
    let items: Array<string> = []
    itemReward.forEach((_, key) => {
        items.push(key);
    });
    result = items.join(", ");
    return result;
}
export function itemListToString(itemList?: ItemList): Array<string> {
    let result: Array<string> = [];
    if (itemList === undefined) return result;
    itemList.forEach((value, key) => {
        result.push(value.toString() + " " + key);
    });
    return result;
}
export function calculateRewards(itemReward?: ItemReward): ItemList {
    const itemList = new Map<string, number>();
    if (itemReward === undefined) return itemList;
    let entries = Array.from(itemReward.entries());
    entries.forEach(([item, reward]) => {
        itemList.set(item, MathUtils.getRandomInt(reward.min, reward.max));
    })
    //get random item
    return itemList;
}
