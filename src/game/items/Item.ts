export type ItemList = Map<string, number>;  //Itemkey, quantity
export type ItemReward = Map<string, {min:number,max:number}>;  //Itemkey, quantity