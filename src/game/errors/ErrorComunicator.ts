export interface ErrorComunicator {
    message: string;
}

export function isErrorComunicator(value: any): value is ErrorComunicator {
    return "message" in value;
}