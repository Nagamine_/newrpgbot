import BattleEntity from './BattleEntity'


// get battleEntity by user
function getBattleEntityByUser(
    entities: Array<BattleEntity>,
    user: string
): BattleEntity | null {
    let battleEntity: BattleEntity | null = null
    for (const entity of entities) {
        if (entity.user === user) {
            battleEntity = entity
            break
        }
    }
    return battleEntity
}

// get battleEntityu by id
function getBattleEntityById(
    entities: Array<BattleEntity>,
    id: string
): BattleEntity | null {
    let battleEntity: BattleEntity | null = null
    for (const entity of entities) {
        if (entity.id === id) {
            battleEntity = entity
            break
        }
    }
    return battleEntity
}
//get entities that are not controlled by ia
function getNonIaEntities(entities: Array<BattleEntity>): Array<BattleEntity> {
    return entities.filter((entity) => entity.controller !== 'IA')
}


//create a function that gets a battle and a entity and returns the ones that have diferent bando
function getEnemyEntities(
    userBattleEntity: BattleEntity,
    entities: Array<BattleEntity>
) {
    let enemyEntities: Array<BattleEntity> = []
    for (const entity of entities) {
        if (entity.bando !== userBattleEntity.bando) {
            enemyEntities.push(entity)
        }
    }
    return enemyEntities
}
function getAllyEntities(
    userBattleEntity: BattleEntity,
    entities: Array<BattleEntity>
) {
    let enemyEntities: Array<BattleEntity> = []
    for (const entity of entities) {
        if (entity.bando === userBattleEntity.bando && entity.id !== userBattleEntity.id) {
            enemyEntities.push(entity)
        }
    }
    return enemyEntities
}
export default {
    getBattleEntityById,
    getBattleEntityByUser,
    getEnemyEntities,
    getNonIaEntities,
    getAllyEntities
}
