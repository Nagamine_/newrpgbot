import { Stats } from "../stats/Stats";
import Skill from "../skills/Skill";
import Status from "../skills/Status";
export type BattleEntityController = 'IA' | 'HUMAN'
export interface BattleEntityBasicInformation {
    user?: string,
    controller: BattleEntityController
    id: string,
    battleId: string
    name: string,
    bando: string,
    stats: Stats,
    skills: Array<Skill|undefined>
}
export interface SkillCooldown {
    [skillName: string]: number,
}

//create an interface whose key is the skill name and the value is the cooldown

export default interface BattleEntity extends BattleEntityBasicInformation {
    skills: Array<Skill>,
    currentStats: Stats,
    selectedSkill: Skill | null,
    selectedObjetives: Array<BattleEntity> | null,
    status: Array<Status>,
    battleId: string,
    turn: {
        cooldowns: SkillCooldown,
        numberTurns: number,
        nextTurn: number,
    }
}

export interface HumanBattleEntity extends BattleEntity {
    controller: 'HUMAN',
    user: string,
}
export interface IABattleEntity extends BattleEntity {
    controller: 'IA',
    user: undefined,
}
