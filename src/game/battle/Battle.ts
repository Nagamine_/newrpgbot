import Skill from "../skills/Skill";
import BattleEntity from "./BattleEntity";
import globalContext from "../../globalContext/GlobalContext";
import GameUtils from "../../utils/GameUtils";
import SkillUtils from "../skills/SkillUtils";
import { EffectTime } from "../skillScripts/SkillStatus";
import { SkillComunicator } from "../skillScripts/SkillScripts";
import { ItemReward } from "../items/Item";
import BattleUtils from "./BattleUtils";
import BattleEntityUtils from "./BattleEntityUtils";
export interface IBattle {
    id: string
    maxParticipants: number,
    entities: Array<BattleEntity>,
    orderedTurns: Array<BattleEntity>,

}
export interface BattleConstructor {
    id?: string
    maxParticipants: number,
    entities?: Array<BattleEntity>,
    reward?: ItemReward
}
export default class Battle implements IBattle {
    id: string;
    maxParticipants: number;
    entities: BattleEntity[];
    orderedTurns: BattleEntity[];
    reward?: ItemReward
    isFinished: boolean = false
    constructor(battle: BattleConstructor = {
        maxParticipants: 1,
    }) {
        this.id = battle.id ?? GameUtils.generateUniqueID()
        this.maxParticipants = battle.maxParticipants
        this.entities = battle.entities ?? []
        this.orderedTurns = battle.entities ?? []
        this.reward = battle.reward
        // this.orderEntitiesByNextTurnWait()
    }
    getUserEntity(userID: string) {
        let userBattleEntity: BattleEntity | null = null
        for (const entity of this.entities) {
            if (entity.user === userID) {
                userBattleEntity = entity
                break
            }
        }
        return userBattleEntity
    }
    executeStatus(entity: BattleEntity, time: EffectTime) {
        let battlelog = ""
        entity.status.forEach(status => {
            if (status.effect.time === time) {
                const skillComunicator = status.effect.execute(entity, status)
                this.dispatchSkillComunicator(entity, skillComunicator, [entity])
                battlelog += SkillUtils.replaceMsg(status.effect.message, {
                    user: entity.name,
                    damage: skillComunicator.damage?.toString() ?? "0",
                    turns: skillComunicator.turns?.toString() ?? "0",
                })
                if (status.turns > 0) {
                    status.turns--
                } else {
                    battlelog += SkillUtils.replaceMsg(status.effect.vanishMsg, {
                        user: entity.name,
                    })
                    entity.status = entity.status.filter(it => it !== status)
                }
            }
        })
        const remainigEntities = this.entities.filter(entity => entity.currentStats.vida > 0)

        if(entity.currentStats.vida <= 0){
            battlelog += `${entity.name} ha muerto a causa de sus heridas\n`
            if(BattleUtils.getAllyEntities(entity, remainigEntities).length === 0){
                this.isFinished = true
            }
        }
        if (BattleUtils.getEnemyEntities(entity, remainigEntities).length === 0 ) {
            this.isFinished = true
        }
        return battlelog
    }
    dispatchSkillComunicator(entity: BattleEntity, skillComunicator: Partial<SkillComunicator>, objetives: BattleEntity[]) {
        const dmg = skillComunicator.damage
        const status = skillComunicator.status
        if (dmg !== undefined) {
            objetives.forEach(it => {
                it.currentStats.vida -= dmg
            })
        }
        if (status) {
            objetives.forEach(it => {
                it.status.push(status)
            })
        }
    }
    handleNextTurn() {
        let battlelog = ""
        const sortedEntities = this.entities.sort((a, b) => b.selectedSkill!.modifier.velocidad - a.selectedSkill!.modifier.velocidad)
        for (const entity of this.entities) {
            if(entity.currentStats.vida >0 && !this.isFinished){
                battlelog += this.executeStatus(entity, EffectTime.START_TURN)
            }
        }
        for (const entity of sortedEntities) {
            if (entity.selectedSkill !== null && entity.selectedObjetives !== null && entity.currentStats && entity.currentStats.vida >0 && !this.isFinished) {
                battlelog += `${entity.name} usó ${entity.selectedSkill.name}\n`
                battlelog += this.executeSkill(entity, entity.selectedSkill, entity.selectedObjetives)
    
                entity.selectedSkill = null
                entity.selectedObjetives = []
            }
        }
        for (const entity of this.entities) {
            if(entity.currentStats.vida >0 && !this.isFinished){
                battlelog += this.executeStatus(entity, EffectTime.END_TURN)
            }
        }
        this.entities = this.entities.filter(entity => entity.currentStats.vida > 0)
        if(this.isFinished){
            battlelog += "La batalla ha terminado\n"
        }
        return battlelog
    }

    executeSkill(entity: BattleEntity, skill: Skill, objetives: BattleEntity[]) {
        entity.turn.cooldowns = BattleEntityUtils.getReducedCooldowns(entity)
        let battlelog = ""
        if (entity.turn.cooldowns[skill.id] === 0) {
            entity.turn.cooldowns[skill.id] = skill.modifier.cooldown
            skill.actions.forEach(action => {
                const attackScript = globalContext.skillScripts.get(action)
                if (attackScript === undefined) {
                    battlelog += "ERROR - no hay attackscript"
                    console.error("ERROR - no hay attackscript")
                    return battlelog
                }
                const skillComunicator = attackScript.execute(entity, objetives, skill.modifier)
                this.dispatchSkillComunicator(entity, skillComunicator, objetives)

                battlelog += SkillUtils.replaceMsg(GameUtils.getRandomItem(skill.messages), {
                    user: entity.name,
                    objetives: objetives.map(it => it.name).join(", "),
                    damage: skillComunicator.damage?.toString() ?? "0",
                    turns: skillComunicator.turns?.toString() ?? "0",
                })
            })
        } else {
            battlelog = `${entity.name} no puede usar ${skill.name} por ${entity.turn.cooldowns[skill.id]} turnos`
        }
        objetives.forEach(it => {
            if (it.currentStats.vida <= 0) {
                battlelog += `${entity.name} ha eliminado a ${it.name}\n`
            }
        })
        const remainigEntities = this.entities.filter(entity => entity.currentStats.vida > 0)
        if (BattleUtils.getEnemyEntities(entity, remainigEntities).length === 0) {
            this.isFinished = true
        }
        return battlelog
    }

    // getNextTurnMove() {

    //     this.orderEntitiesByNextTurnWait()
    //     const currentEntity = this.orderedTurns.pop()
    //     if (!currentEntity) {
    //         console.error("Error - BattleManager.ts - getNextTurnMove - no hay entidades")
    //     } else {
    //         currentEntity.turn.numberTurns++
    //         this.orderedTurns.forEach(entity => {
    //             entity.turn.nextTurn += entity.currentStats.velocidad
    //         })
    //         currentEntity.turn.nextTurn = currentEntity.currentStats.velocidad
    //         this.orderedTurns.unshift(currentEntity)
    //     }
    //     return currentEntity!!
    // }
    //La funcion ordena eñ array del mas lento al mas rapido, para poder sacarlos con pop
    // orderEntitiesByNextTurnWait() {
    //     this.orderedTurns.sort((a, b) => {
    //         const speedA = a.turn.nextTurn ?? a.currentStats.velocidad
    //         const speedB = b.turn.nextTurn ?? b.currentStats.velocidad
    //         return speedA - speedB
    //     })
    // }
    serialize() {
        return {
            id: this.id,
            maxParticipants: this.maxParticipants,
            entities: this.entities,
            reward: this.reward
        }
    }
}
