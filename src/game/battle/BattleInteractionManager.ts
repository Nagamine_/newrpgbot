import { ButtonInteraction, StringSelectMenuInteraction } from "discord.js";
import createEmbedBattle from "../../discord/Interactions/battleInteractions/CreateEmbedBattle";
import Skill, { SkillTarget } from "../skills/Skill";
import Battle from "./Battle";
import BattleContext from "./BattleContext";
import BattleEntity, { HumanBattleEntity } from "./BattleEntity";
import BattleIA from "./BattleIA";
import BattleUtils from "./BattleUtils";
import BattleBD from "../../repository/BattleBD";
import BattleEntityUtils from "./BattleEntityUtils";
import CharacterDataDB from "../../repository/CharacterDataDB";
import InventoryDB from "../../repository/InventoryDB";
import { calculateRewards, itemListToString } from "../items/ItemUtils";
import { addItemsToInventory } from "../character/Inventory";
import createObjetivesButtons from "../../discord/Interactions/battleInteractions/CreateObjetivesButtons";
import createSkillButtons from "../../discord/Interactions/battleInteractions/CreateSkillButtons";

export type Interactable = ButtonInteraction | StringSelectMenuInteraction
//a class that handles all the interactions in the battle scene
export interface BattleInteractionContext {
    battle: Battle;
    interaction: Interactable;
}
export interface BattleInteractionEntityContext extends BattleInteractionContext {
    battleEntity: BattleEntity;
}
export interface BattleInteractionSkillContext extends BattleInteractionEntityContext {
    skill: Skill;
}
export interface BattleInteractionObjetiveContext extends BattleInteractionEntityContext {
    objetive: BattleEntity;
}
export interface BattleInteractionSkillObjetiveContext extends BattleInteractionSkillContext {
    objetives: Array<BattleEntity>;
}

//NOTA: considerar evaluar battlebd.save luego de cada comando ya que puede traer condiciones de carrera
const replyTime = 5000
async function joinBattle(ctx: BattleInteractionEntityContext) {
    if (ctx.battle.maxParticipants < BattleUtils.getNonIaEntities(ctx.battle.entities).length) {
        ctx.interaction.reply({
            content: `No hay mas espacio en el combate`,
        })
        return
    }
    // check if entity is already in battle
    if (ctx.battle.entities.find(e => e.user === ctx.battleEntity.user) !== undefined) {
        ctx.interaction.reply({
            content: `ya estas en el combate`,
            ephemeral: true,
        })
        return
    }
    if (ctx.battleEntity.controller === "HUMAN" && ctx.battleEntity.user !== null) {
        const human = ctx.battleEntity as HumanBattleEntity
        BattleContext.userIdToInteractions.set(human.user, ctx.interaction)
        await BattleBD.addUsertoBattle(ctx.battle.id, human.user)
    }
    ctx.battle.entities.push(ctx.battleEntity)
    await BattleBD.save(ctx.battle)
    ctx.interaction.update({
        content
            : `${ctx.interaction.message}\n- ${ctx.battleEntity.name}`,
    })
}
async function startBattle(ctx: BattleInteractionContext) {
    if (!ctx.interaction.channel) return
    BattleContext.userIdToInteractions.set(ctx.interaction.user.id, ctx.interaction)
    await BattleBD.save(ctx.battle)
    await ctx.interaction.update({
        components: [],
    })
    // ctx.interaction.channel.send(createEmbedBattle(ctx.battle))
    //await this.interaction.channel.send(JsonUtils.stringify(this.battleManager.battle.entities))
    selectSkillsPhase(ctx)
}
//This function iterates over the entities and sends an interaction if the entity is human or executes the IA action
async function selectSkillsPhase(ctx: BattleInteractionContext) {
    if (!ctx.interaction.channel) return
    await ctx.interaction.channel.send(createEmbedBattle(ctx.battle))
    for (const entity of ctx.battle.entities) {
        if (entity.controller === "HUMAN" && entity.user !== null) {
            const human = entity as HumanBattleEntity
            const interaction = BattleContext.userIdToInteractions.get(human.user)
            if (interaction === undefined) {
                console.error("error - BattleInteractionManager.ts - no battle interaction")
                errorBattle(ctx)
                return
            }
            await interaction.followUp(createSkillButtons(human))
            // clearTimeout(BattleContext.userIdToTimeout.get(human.user)!)
            // const time = setTimeout(async() => {
            //     console.log("timeout")
            //     const skillContext: BattleInteractionSkillContext = {
            //         ...ctx,
            //         battleEntity: entity,
            //         skill: entity.skills.find(s => s.id === "passTurn") as Skill,
            //     }
            //     const battle = await BattleBD.read(ctx.battle.id)
            //     console.log(battle!.entities.find(e => entity.id === e.id))
            //     if (  battle!.entities.find(e => entity.id === e.id)!.selectedSkill === null) {
            //         timeoutInteraction(ctx.interaction, () => skillSelect(skillContext))
            //     }
            // }, replyTime)
            // BattleContext.userIdToTimeout.set(human.user, time)
        } else {
            const skill = BattleIA.battleIAAttack(entity)
            skillSelect({
                ...ctx,
                battleEntity: entity,
                skill,
            })
        }
    }
    await BattleBD.save(ctx.battle)
}
// async function nextTurn(ctx: BattleInteractionContext) {
//     const battleEntity = ctx.battle.getNextTurnMove()
//     if (battleEntity.controller === "IA") {
//         const skill = BattleIA.battleIAAttack(battleEntity)
//         skillSelect({
//             ...ctx,
//             battleEntity,
//             skill,
//         })
//     } else {
//         if (battleEntity.controller === "HUMAN" && battleEntity.user !== null) {
//             const human = battleEntity as HumanBattleEntity
//             await ctx.interaction.channel?.send(createEmbedBattle(ctx.battle))
//             //await this.interaction.channel.send(JsonUtils.stringify(this.battleManager.battle.entities))
//             const interact = BattleContext.userIdToInteractions.get(human.user)
//             if (interact === undefined) {
//                 console.error("error - BattleInteractionManager.ts - no battle interaction")
//                 return
//             }
//             await ctx.interaction.channel?.send(`Es el turno de ${interact.user}`)
//             await interact.followUp(createSkillButtons(human))
//         }
//     }
// }
async function skillSelect(ctx: BattleInteractionSkillContext) {
    if (!ctx.interaction.channel) return
    const objetives = BattleUtils.getEnemyEntities(ctx.battleEntity, ctx.battle.entities)
    ctx.battleEntity.selectedSkill = ctx.skill

    const isMultiTarget = ctx.skill.target === SkillTarget.SINGLE && objetives.length >= 2
    if (!isMultiTarget) {
        ctx.battleEntity.selectedObjetives = objetives
    }
    if (ctx.battleEntity.controller === "IA" && isMultiTarget) {
        const objetive = BattleIA.selectObjetive(ctx.battle, ctx.battleEntity)
        ctx.battleEntity.selectedObjetives = [objetive]
    }
    if (BattleEntityUtils.isHuman(ctx.battleEntity)) {
        BattleContext.userIdToInteractions.set(ctx.battleEntity.user, ctx.interaction)
        if (isMultiTarget) {
            await BattleBD.save(ctx.battle)
            await ctx.interaction.update(createObjetivesButtons(objetives))
            // clearTimeout(BattleContext.userIdToTimeout.get(ctx.battleEntity.user)!)
            // const time = setTimeout(() => {
            //     const objetiveContext: BattleInteractionObjetiveContext = {
            //         ...ctx,
            //         objetive: objetives[Math.floor(Math.random() * objetives.length)],
            //     }
            //     if (ctx.battleEntity.selectedObjetives === null
            //         ) {
            //         timeoutInteraction(ctx.interaction, () => objetiveSelect(objetiveContext))
            //     }
            // }, replyTime)
            // BattleContext.userIdToTimeout.set(ctx.battleEntity.user, time)
            return
        } else {
            await ctx.interaction.update({
                content: "Esperando al resto de los jugadores",
                components: []
            })
        }
    }
    await BattleBD.save(ctx.battle)
    validateIfAllEntitiesAreReady({ ...ctx })

}
async function objetiveSelect(ctx: BattleInteractionObjetiveContext) {
    if (ctx.battleEntity.controller === "IA") {
        ctx.interaction.followUp({
            content: `ERROR -eres una IA`,
        })
        errorBattle(ctx)
        return
    }

    const skill = ctx.battleEntity.selectedSkill
    if (skill === null) {
        console.error("ERROR - BattleInteractionManager.ts - objetiveSelect - skill null")
        errorBattle(ctx)
        return
    }
    ctx.battleEntity.selectedObjetives = [ctx.objetive]
    const human = ctx.battleEntity as HumanBattleEntity
    BattleContext.userIdToInteractions.set(human.user, ctx.interaction)
    await BattleBD.save(ctx.battle)
    validateIfAllEntitiesAreReady({ ...ctx })
    // executeSkill({
    //     ...ctx,
    //     skill,
    //     objetives: [ctx.objetive],
    // })
}

async function validateIfAllEntitiesAreReady(ctx: BattleInteractionContext) {
    if (!ctx.interaction.channel) return
    if (ctx.battle.entities.every(e => e.selectedSkill !== null && e.selectedObjetives !== null)) {
        await ctx.interaction.channel.send({
            content: `Todos los jugadores han seleccionado sus habilidades`,
        })
        await ctx.interaction.channel.send(createEmbedBattle(ctx.battle))
        //await this.interaction.channel.send(JsonUtils.stringify(this.battleManager.battle.entities))
        executeNextTurn(ctx)
    }
}
//Iterate over the entities and execute the skills in order
async function executeNextTurn(ctx: BattleInteractionContext) {
    if (!ctx.interaction.channel) return
    if (ctx.battle.entities.findIndex(e => e.selectedSkill === null || e.selectedObjetives === null) > -1) {
        console.error("BattleInteractionManager.ts - executeAllSkillsInOrder - selected skill or objetives null")
        errorBattle(ctx)
        return
    }
    let battlelog = ctx.battle.handleNextTurn()
   
    if (battlelog.length > 0) {
        await ctx.interaction.channel.send(battlelog)
    }
    await BattleBD.save(ctx.battle)
    if (!ctx.battle.isFinished) {
        selectSkillsPhase(ctx)
    } else {
        endBattle(ctx)
    }
}
// async function executeSkill(ctx: BattleInteractionSkillObjetiveContext) {
//     if (!ctx.interaction.channel) return
//     ctx.battleEntity.turn.cooldowns = BattleEntityUtils.getReducedCooldowns(ctx.battleEntity)
//     if(ctx.battleEntity.turn.cooldowns[ctx.skill.id] > 0) {
//         await ctx.interaction.channel.send({
//             content: `${ctx.battleEntity.name} no puede usar ${ctx.skill.name} por ${ctx.battleEntity.turn.cooldowns[ctx.skill.id]} turnos`
//         })

//     }else{
//         ctx.battleEntity.turn.cooldowns[ctx.skill.id] = ctx.skill.modifier.cooldown
//         const battlelog = ctx.battle.executeSkill(ctx.battleEntity, ctx.skill, ctx.objetives)
//         await ctx.interaction.channel.send({
//             content: `${ctx.battleEntity.name} usó ${ctx.skill.name}`
//         })
//         if (battlelog.length > 0) {
//             await ctx.interaction.channel.send(battlelog)
//         }
//     }
//     await BattleBD.save(ctx.battle)
//     if (BattleUtils.getEnemyEntities(ctx.battleEntity, ctx.battle.entities).length === 0) {
//         endBattle(ctx)
//     } else {
//         nextTurn(ctx)
//     }
// }
async function endBattle(ctx: BattleInteractionContext) {
    await ctx.interaction.followUp("El combate ha terminado")
    ctx.battle.entities.forEach(human => {
        if (BattleEntityUtils.isHuman(human)) {
            CharacterDataDB.read(human.user).then(async (char) => {
                if (char) {
                    char.alive = char.statsModifier.vida + human.stats.vida > 0
                    char.statsModifier = BattleEntityUtils.getStatsModifier(human)
                    CharacterDataDB.create(char)
                    if (ctx.battle.reward !== undefined) {
                        const inventory = await InventoryDB.read(human.user)
                        if (inventory) {
                            const reward = calculateRewards(ctx.battle.reward)
                            InventoryDB.create(addItemsToInventory(inventory, reward))
                            const reply = `${human.name} ha obtenido:\n>>> ${itemListToString(reward).join('\n')}`
                            ctx.interaction.followUp(reply)

                        }
                    }
                }
            })
        }
    })
    BattleBD.remove(ctx.battle.id)
}
async function errorBattle(ctx: BattleInteractionContext) {
    await ctx.interaction.followUp("ERROR - El combate ha terminado")
    BattleBD.remove(ctx.battle.id)
}
// async function timeoutInteraction(interaction: Interactable, nextStep: () => void) {
//     interaction.reply("No respondiste a tiempo")
//     nextStep()
//     console.log("TIMEOUT")
// }
export default {
    joinBattle,
    startBattle,
    // nextTurn,
    skillSelect,
    // executeSkill,
    endBattle,
    objetiveSelect,
}