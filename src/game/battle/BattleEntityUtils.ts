import GlobalContext from "../../globalContext/GlobalContext"
import GameUtils from "../../utils/GameUtils"
import Skill from "../skills/Skill"
import { Stats } from "../stats/Stats"
import { EnemyPrototype } from "../world/EnemyPrototype"
import BattleEntity, { BattleEntityBasicInformation, HumanBattleEntity, SkillCooldown } from "./BattleEntity"

function create(battleEntity: BattleEntityBasicInformation): BattleEntity{
    const skills = battleEntity.skills.filter(skill => skill !== undefined) as Array<Skill>
    skills.push(GlobalContext.skills.get("passTurn") as Skill)
    if(battleEntity.skills.some(skill => skill === undefined)){
        console.error("ERROR - BattleEntityUtils.create - Some skills are undefined")
    }
    const cooldowns: SkillCooldown = {}
    skills.forEach(skill => {
        cooldowns[skill.id] = 0
    })
    const entity: BattleEntity =  {
        ...battleEntity,
        skills: skills,
        currentStats: structuredClone(battleEntity.stats),
        turn: {
            cooldowns: cooldowns,
            numberTurns: 0,
            nextTurn: battleEntity.stats.velocidad
        },
        selectedSkill: null,
        selectedObjetives: null,
        status: []
    }
    return entity
}
function createFromPrototype(prototype: EnemyPrototype,battleId: string, bando: string = "2"): BattleEntity{
    return create({
        name: prototype.name,
        id: GameUtils.generateUniqueID(),
        controller: 'IA',
        battleId: battleId,
        bando: "2",
        stats: prototype.stats,
        skills: prototype.skills
    })
}
//TODO - TEMPORAL FUNCT UNTIL THE REFACTOR
function getStatsModifier(entity: BattleEntity): Stats {
    return {
        vida: entity.currentStats.vida - entity.stats.vida,
        fuerza: entity.currentStats.fuerza - entity.stats.fuerza,
        velocidad: entity.currentStats.velocidad - entity.stats.velocidad,
        defensa: entity.currentStats.defensa - entity.stats.defensa
    }
}

function getReducedCooldowns(entity: BattleEntity): SkillCooldown{
    const newCooldowns: SkillCooldown = {}
    for(const skillName in entity.turn.cooldowns){
        newCooldowns[skillName] = Math.max(0, entity.turn.cooldowns[skillName] - 1)
    }
    return newCooldowns
}
function isHuman(entity: BattleEntity): entity is HumanBattleEntity{
    return entity.controller === 'HUMAN'
}
function isIA(entity: BattleEntity): entity is HumanBattleEntity{
    return entity.controller === 'IA'
}
export default {
    create,
    createFromPrototype,
    getStatsModifier,
    getReducedCooldowns,
    isHuman,
    isIA
}