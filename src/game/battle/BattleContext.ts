import Battle from "./Battle";
import { Interactable } from "./BattleInteractionManager";
const BattleContext = {
    userIdToInteractions: new Map<string, Interactable>(), //userID-interactable
    userIdToTimeout: new Map<string, NodeJS.Timeout>(), //userID-timeout
    userIdToBattle: new Map<string, string>(), //userID-battleID
    battles: new Map<string, Battle>(), //battleID-battle
}

export default BattleContext