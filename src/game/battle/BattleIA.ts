import Battle from "./Battle";
import BattleEntity from "./BattleEntity";
import BattleUtils from "./BattleUtils";

//function that calculates the best move for the IA
export function battleIAAttack(battleEntity: BattleEntity){
    const skills = battleEntity.skills
    const selectedSkill = skills[Math.floor(Math.random() * skills.length)]
    return selectedSkill
}
export function selectObjetive(battle: Battle,battleEntity: BattleEntity){
    const enemies = BattleUtils.getEnemyEntities(battleEntity,battle.entities)
    const objetive = enemies[Math.floor(Math.random() * enemies.length)]
    return objetive
}
export default {
    battleIAAttack,
    selectObjetive
}