import SkillModifier from "./SkillModifier";

export enum SkillTarget {
    SINGLE = "single",
    All = "all",
  }
export default interface Skill {
    id: string,
    name: string,
    description: string,
    target: SkillTarget,
    modifier: SkillModifier,
    actions: string[],
    messages: string[]
}