import SkillStatus from "../skillScripts/SkillStatus";
import SkillModifier from "./SkillModifier";

export default interface Status {
    effect: SkillStatus,
    turns: number,
    modifier: SkillModifier,
    power: number,
}