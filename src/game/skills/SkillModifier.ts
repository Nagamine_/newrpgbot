export default interface SkillModifier {
    poder: number,
    turnos?: number,
    velocidad: number,
    cooldown: number,
}