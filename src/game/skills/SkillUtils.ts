interface MsgKey {
    user:string,
    objetives: string,
    damage: string,
    turns: string,
  }
const replaceMsg = (template: string,replace:Partial<MsgKey>)=>{
    let string = template
    Object.entries(replace).forEach(([key, value]) => {
            string = string.replace(`{${key}}`,value)
    })
    string = string+"\n"
    return string
}
// const getCooldownsFromSkills = (skills: Array<Skill>) => {
//     const cooldowns: SkillCooldown = {}
//     skills.forEach(skill => {
//         cooldowns[skill.id] = skill.modifier.cooldown
//     })
//     return cooldowns
// }
export default {
    replaceMsg,
    // getCooldownsFromSkills
}