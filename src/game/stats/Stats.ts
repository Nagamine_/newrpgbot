export interface Stats {
    vida: number,
    fuerza: number,
    velocidad: number,
    defensa: number
}