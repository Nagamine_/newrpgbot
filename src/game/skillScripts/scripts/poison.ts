import BattleEntity from '../../battle/BattleEntity'
import SkillModifier from '../../skills/SkillModifier'
import SkillScript, { SkillComunicator } from '../SkillScripts'
import poisonScript from '../statusScripts/poisonScript'

function execute(
    self: BattleEntity,
    objetive: Array<BattleEntity>,
    modifier: SkillModifier
):SkillComunicator {
    return {
        status: {
            effect: poisonScript,
            turns: modifier.turnos ?? 0,
            modifier: modifier,
            power: modifier.poder * self.currentStats.fuerza ?? 0,
        },
        turns:modifier.turnos,
    }

}
const poison: SkillScript = {
    execute: execute,
}
export default poison
