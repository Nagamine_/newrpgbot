import BattleEntity from '../../battle/BattleEntity'
import SkillModifier from '../../skills/SkillModifier'
import SkillScript, { SkillComunicator } from '../SkillScripts'
//TODO cambiar posicion de objetive por modifier
function execute(
    self: BattleEntity,
    objetive: Array<BattleEntity>,
    modifier: SkillModifier
):SkillComunicator {
    return {
        damage: self.currentStats.fuerza * modifier.poder,
    }

}
const basicDamage: SkillScript = {
    execute: execute,
}
export default basicDamage
