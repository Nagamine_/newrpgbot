import BattleEntity from "../battle/BattleEntity";
import SkillModifier from "../skills/SkillModifier";
import Status from "../skills/Status";
import { SkillComunicator } from "./SkillScripts";

export enum EffectTime {
    START_TURN = "startTurn",
    END_TURN = "endTurn"
}
export default interface SkillStatus {
    name: string,
    time: EffectTime,
    message: string,
    vanishMsg: string,
    execute: (self:BattleEntity, status: Status) => Partial<SkillComunicator>, 
}