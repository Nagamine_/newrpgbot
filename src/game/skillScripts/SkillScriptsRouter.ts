import { Collection } from "discord.js";
import path from "path";
import SkillScript from "./SkillScripts";
import fs from "fs";
export function loadSkillScripts(){
    let scripts = new Collection<string,SkillScript>();
    const pathDir = path.join(__dirname,'scripts');
    const files = fs.readdirSync(pathDir).filter(file => file.endsWith('.ts') || file.endsWith('.js'));
    for (const file of files) {
        const filePath = path.join(pathDir, file);
        const script = import(filePath);
        script.then((it)=>{
            let comando:SkillScript = it.default
            scripts.set(file.split(".")[0],comando)
        })
        // Set a new item in the Collection
        // With the key as the command name and the value as the exported module
        //client.commands.set(command.data.name, command);
    }
    console.log("loaded skill scripts")
    return scripts
}
