import BattleEntity from '../../battle/BattleEntity'
import Status from '../../skills/Status'
import { SkillComunicator } from '../SkillScripts'
import SkillStatus, { EffectTime } from '../SkillStatus'

function execute(
    self: BattleEntity,
    status: Status
):Partial<SkillComunicator> {
    return {
        damage: status.power,
        turns: status.turns,
    }
}
const poisonScript: SkillStatus = {
    name: "poison",
    time: EffectTime.END_TURN,
    message: "{user} recibió {damage} de daño por veneno, {turns} turnos restantes",
    vanishMsg: "{user} ya no está envenenado",
    execute: execute,
}
export default poisonScript
