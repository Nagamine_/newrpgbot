import BattleEntity from "../battle/BattleEntity";
import Status from "../skills/Status";
import SkillModifier from "../skills/SkillModifier";
export interface SkillComunicator {
    damage?: number,
    status?: Status,
    turns?: number,
}
export default interface SkillScript {
    
    execute: (self:BattleEntity, objetive:Array<BattleEntity>, modifier: SkillModifier) => SkillComunicator,
}