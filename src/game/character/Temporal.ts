export default interface Temporal {
    [key:string]: number;
}