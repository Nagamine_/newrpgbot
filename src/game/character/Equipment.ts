import { Stats } from "../stats/Stats";
export type ArmorPart = "cabeza" | "pecho" | "piernas"| "pies" | "mano"
export type BodyPart = "cabeza" | "pecho" | "piernas"| "pies" | "manoI" | "manoD"
export default interface Equipment {
    name: string
    bodyPart: ArmorPart
    stats: Partial<Stats>
}

