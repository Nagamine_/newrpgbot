import SkillModifier from "../skills/SkillModifier";
import BattleEntity, { BattleEntityBasicInformation } from "../battle/BattleEntity";
import { Stats } from "../stats/Stats";
import Character from "./Character";
import BattleEntityUtils from "../battle/BattleEntityUtils";
import CharacterData from "./CharacterData";
import GlobalContext from "../../globalContext/GlobalContext";

//convert character to battle entity merging the equipment stats
export function characterToBattleEntity(character: Character,characterData:CharacterData,battleId: string): BattleEntity {
    const battleEntity: BattleEntityBasicInformation = {
        id: character.user,
        controller: 'HUMAN',
        battleId: battleId,
        name: character.name,
        user: character.user,
        bando: "player",
        stats: {
            vida: 0,
            fuerza: 0,
            velocidad: 0,
            defensa: 0
        },
        skills: []
    };
    character.equipment.forEach((equipment) => {
        battleEntity.stats.vida += equipment.stats.vida ?? 0;
        battleEntity.stats.fuerza += equipment.stats.fuerza ?? 0;
        battleEntity.stats.velocidad += equipment.stats.velocidad ?? 0;
        battleEntity.stats.defensa += equipment.stats.defensa ?? 0;
        battleEntity.skills = character.skills.map(skill => GlobalContext.skills.get(skill))
    });

    const be = BattleEntityUtils.create(battleEntity);
    be.currentStats.vida += characterData.statsModifier.vida 
    be.currentStats.fuerza += characterData.statsModifier.fuerza
    be.currentStats.velocidad += characterData.statsModifier.velocidad
    be.currentStats.defensa += characterData.statsModifier.defensa
    return be
}

export function profileStatsToString(stats: Partial<Stats>,currentStats: Partial<Stats>,): string {
    let c = '';
    for (const stat in stats) {
        const key = stat as keyof Stats;
        c +=  `\n${stat}: `
        if(key in currentStats && currentStats[key] !== stats[key] ){
            c +=  `${currentStats[key]}/`
        }
        c += stats[key]
        //stat: 200/1000
    }
    return c;
}
export function statsToString(stats: Partial<Stats>): string {
    let c = '';
    for (const stat in stats) {
        const key = stat as keyof Stats;
        c += '\n' + stat + ': ' + stats[key];
    }
    return c;
}

export function modifiersToString(modifier: Partial<SkillModifier>): string {
    let c = '';
    for (const modif in modifier) {
        const key = modif as keyof SkillModifier;
        c += '\n' + modif + ': ' + modifier[key];
    }
    return c;
}
