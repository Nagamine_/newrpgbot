import { ItemList } from "../items/Item";

export default interface Inventory {
    userId: string,
    items: ItemList
}

export function addItemsToInventory(inventory:Inventory, items: ItemList):Inventory{
    items.forEach((value, key) => {
        const item = inventory.items.get(key)
        if(item === undefined){
            inventory.items.set(key, value)
        }else{
            inventory.items.set(key, item + value)
        }
    });
    return inventory;
}
export function removeItemsFromInventory(inventory:Inventory, items: ItemList):Inventory{
    items.forEach((value, key) => {
        const item = inventory.items.get(key)
        //Remove value, if values es less than 1 remove item
        if(item === undefined || item - value < 1){
            inventory.items.delete(key)
        }else{
            inventory.items.set(key, item - value)
        }
    });
    return inventory;
}

export function validateIfInventoryContainsItems(inventory: Inventory, itemList: ItemList,): boolean {
    let result = true;
    itemList.forEach((value, key) => {
        const inventoryValue = inventory.items.get(key);
        if (inventoryValue === undefined || inventoryValue < value) result = false;
    });
    return result;
}

export function getAmmountOfItems(inventory: Inventory, items: Array<string> ): ItemList {
    const itemList = new Map<string, number>();
    items.forEach(item => {
        const itemAmmount = inventory.items.get(item);
        if(itemAmmount !== undefined){
            itemList.set(item, itemAmmount)
        }else{
            itemList.set(item, 0)
        }
    });
    return itemList;
}
export function getMissingItems(inventory: Inventory, items: ItemList): ItemList {
    const itemList = new Map<string, number>();
    items.forEach((value, key) => {
        const itemAmmount = inventory.items.get(key) ?? 0;
        if(itemAmmount < value){
            itemList.set(key, value - itemAmmount)
        }
    });
    return itemList;
}