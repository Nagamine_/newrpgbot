import { FIRST_LOCATION } from "../../utils/Constants";
import { Stats } from "../stats/Stats";

export default interface CharacterData{
    userId: string,
    currentLocation: Map<string, string>, // serverID, locationID
    statsModifier: Stats;
    alive: boolean;
}

export function getCurrentLocation(characterData: CharacterData,serverID: string | null): string{
    if(serverID == null) {
        console.error("ERROR - CharacterData.ts - serverID null")
        return FIRST_LOCATION;
    }
    return characterData.currentLocation.get(serverID) ?? FIRST_LOCATION;
}