import Equipment from "./Equipment";

export default interface Archetipe {
    name: string,
    description: string,
    equipo: Equipment[],
}