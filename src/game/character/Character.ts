import CharacterDataDB from "../../repository/CharacterDataDB";
import CharactersDB from "../../repository/CharactersDB";
import InventoryDB from "../../repository/InventoryDB";
import CharacterData from "./CharacterData";
import Equipment, { BodyPart } from "./Equipment";
import Inventory from "./Inventory";

export default interface Character {
    name: string;
    user: string;
    equipment: Map<BodyPart, Equipment>;
    skills: Array<string>;
}

export async function createCharacter(username: string, userId: string): Promise<{ character: Character; characterData: CharacterData; inventory: Inventory; }> {
    const equipment = new Map<BodyPart, Equipment>()

    equipment.set("cabeza", {
        name: "casco basico",
        bodyPart: "cabeza",
        stats: {
            vida: 200,
            fuerza: 5,
        }
    })
    equipment.set("pecho", {
        name: "pecho basico",
        bodyPart: "pecho",
        stats: {
            vida: 600,
            fuerza: 1,
        }
    })
    equipment.set("piernas", {
        name: "pantalon basico",
        bodyPart: "pecho",
        stats: {
            vida: 200,
        }
    })
    equipment.set("pies", {
        name: "zapatos basicos",
        bodyPart: "pies",
        stats: {
            vida: 200,
            velocidad: 8
        }
    })
    equipment.set("manoI", {
        name: "cuchillo",
        bodyPart: "mano",
        stats: {
            vida: 200,
        }
    })
    const items = new Map<string, number>()
    items.set("oro", 2000)
    const character: Character = {
        name: username,
        user: userId,
        equipment: equipment,
        skills: ["cuchillazo", "envenenamiento"]
    }
    const inventory: Inventory =  {
        userId: userId,
        items: items
    }
    const characterData: CharacterData = {
        userId: userId,
        currentLocation: new Map<string, string>(),
        alive: true,
        statsModifier: {
            vida: 0,
            fuerza: 0,
            velocidad: 0,
            defensa: 0,
        }
    }
    await Promise.all([
    CharactersDB.create(character),
    InventoryDB.create(inventory),
    CharacterDataDB.create(characterData)])
    return{
        character,characterData,inventory
    }
}