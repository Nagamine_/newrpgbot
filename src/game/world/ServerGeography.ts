import GlobalContext from "../../globalContext/GlobalContext";
import { FIRST_BUILDING, FIRST_LOCATION } from "../../utils/Constants";
import Building, { BuildingID } from "./Building";
import BuildingTemplate from "./BuildingTemplate";
import Location, { LocationID } from "./Location";


export default interface ServerGeography {
    serverId: string,
    discoveredLocations: Map<LocationID,Location> // locationID, locationServerData
    buildings: Map<BuildingID, Building> // buildingID, buildingTemplate
}

export function createServerGeography(serverID: string): ServerGeography {
    const map = new Map<BuildingID, Building>()
    const buildingPrototype = GlobalContext.buildings.get(FIRST_BUILDING) as BuildingTemplate
    console.log(GlobalContext.buildings)
    if(buildingPrototype === undefined) {
        console.error("ERROR - LocationServerData.ts - buildingPrototype no existente")
    }
    const building: Building = {
        id: buildingPrototype.id,
        name: buildingPrototype.levels[0].name,
        description: buildingPrototype.levels[0].description,
        level: 1,
    }
    map.set(building.id, building)

    const locations = new  Map<LocationID,Location> 
    const locactionPrototype = GlobalContext.locations.get(FIRST_LOCATION) as Location
    if(locactionPrototype === undefined) {
        console.error("ERROR - LocationServerData.ts - locactionPrototype no existente")
    }
    locations.set(FIRST_LOCATION, locactionPrototype)
    return {
        serverId: serverID,
        discoveredLocations: locations,
        buildings: map
    }
}