import { LevelTemplate } from "./BuildingTemplate";

export default interface Building {
    id: BuildingID;
    name: string;
    description: string;
    level: number;
}
export interface BuildingWithLevels extends Building {
    levelDescription: LevelTemplate
}

export type BuildingID = string