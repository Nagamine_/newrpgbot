import GlobalContext from "../../globalContext/GlobalContext";
import InventoryDB from "../../repository/InventoryDB";
import ServerGeographyDB from "../../repository/ServerGeographyDB.";
import Inventory, { getAmmountOfItems, getMissingItems, removeItemsFromInventory, validateIfInventoryContainsItems } from "../character/Inventory";
import { ErrorComunicator } from "../errors/ErrorComunicator";
import { ItemList } from "../items/Item";
import { itemListToString } from "../items/ItemUtils";
import { BuildingWithLevels } from "./Building";
import BuildingTemplate, { LevelTemplate } from "./BuildingTemplate";
import ServerGeography from "./ServerGeography";
export interface BuildingLevel {
    id: string;
    levelTemplate: LevelTemplate
}
function getNextBuildingsList(geography: ServerGeography): Array<BuildingLevel>{
    const buildings: Array<BuildingLevel> = []
    GlobalContext.buildings.forEach(building => {
        const currentBuilding = geography.buildings.get(building.id)
        console.log(currentBuilding)
        let level = 0
        if (currentBuilding !== undefined) {
            if (currentBuilding.level >= building.levels.length ) {
                level = -1
            } else {
                level = currentBuilding.level //Get index of next level
            }
        }
        console.log(building.levels[level])
        if (level > -1) {
            const latestBuilding = building.levels[level]
            buildings.push({
                id: building.id,
                levelTemplate: latestBuilding,
            })
        }
    })
    return buildings
}
function buildBuilding(buildingId: string, geography: ServerGeography, inventory: Inventory) : BuildingWithLevels | ErrorComunicator {
    let currentBuilding = geography.buildings.get(buildingId)
    const building = GlobalContext.buildings.find(building => building.id === buildingId)
    console.log(building)
    console.log(currentBuilding)
    console.log(buildingId)
    if (building === undefined) {
        console.error("Error - BuildingController.ts - buildBuilding - No se ha encontrado el edificio con id: " + buildingId)
        return {message: "Error, id invalido"}
    }
    const maxLevel = building.levels.length
    if(currentBuilding === undefined) {
        currentBuilding = {
            id: buildingId,
            name: building.name,
            description: building.description,
            level: 0,
        }
    }
    if(currentBuilding.level >= maxLevel) {
        console.error("Error - BuildingController.ts - buildBuilding - El edificio con id: " + buildingId + " ya está al máximo nivel")
        return  {message:"Error, max level"}
    }
    const currentLevel = currentBuilding.level
    if(validateIfInventoryContainsItems(inventory,building.levels[currentLevel].cost)) {
        currentBuilding.level++
        removeItemsFromInventory(inventory, building.levels[currentLevel].cost)
        geography.buildings.set(buildingId, currentBuilding)
        ServerGeographyDB.create(geography)
        InventoryDB.create(inventory)
        return {
            ...currentBuilding,
            levelDescription: building.levels[currentLevel],
        }
    }else{
        const items = getMissingItems(inventory, building.levels[currentLevel].cost)
        return{message: "No tienes los items necesarios, te falta: " + itemListToString(items).join(", ")}
    }
  

}
export default {
    getNextBuildingsList,
    buildBuilding
}