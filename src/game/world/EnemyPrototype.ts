import { ItemReward } from "../items/Item";
import Skill from "../skills/Skill";
import { Stats } from "../stats/Stats";

export interface EnemyPrototype {
    name: string;
    stats: Stats;
    items: ItemReward;
    skills: Array<Skill>;
}