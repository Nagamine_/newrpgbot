import { ItemReward } from "../items/Item";
import { EnemyPrototype } from "./EnemyPrototype";

export default interface Location {
    id: LocationID;
    name: string;
    description: string;
    enemies: Array<EnemyPrototype>
    rewardItems: ItemReward
    salary: number
}
export type LocationID = string