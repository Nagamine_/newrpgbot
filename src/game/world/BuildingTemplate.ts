import { ItemList } from "../items/Item";
import { BuildingID } from "./Building";
export interface LevelTemplate{
    cost: ItemList,
    name: string,
    description: string,
}
export default interface BuildingTemplate{
    id: BuildingID,
    name: string,
    description: string,
    levels: Array<LevelTemplate>,
}