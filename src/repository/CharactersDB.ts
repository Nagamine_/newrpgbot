import Character from '../game/character/Character'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
const create = async (character: Character) => {
    await redisClient.set('user:'+character.user,JsonUtils.stringify(character))
}
const read = async (user_id: string): Promise<Character | undefined> => {
    const userStr = await redisClient.get('user:'+user_id)
    if (userStr !== undefined && userStr !== null) {
        return  JsonUtils.parse(userStr)
    }
    return undefined
}

export default { create, read }
