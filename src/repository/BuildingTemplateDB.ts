import { Collection } from 'discord.js';
import fs from 'fs'
import path from 'path'
import BuildingTemplate from '../game/world/BuildingTemplate';
import JsonUtils from '../utils/JsonUtils';
function read() {
    // Blocks the event loop
    const collection = new Collection<string,BuildingTemplate>()
    const buildings = JsonUtils.parse(fs.readFileSync(path.join(__dirname,"../../simulatedDB/buildings.json"), 'utf8'))
    buildings.forEach( (element : BuildingTemplate) => {
        collection.set(element.id, element)
    });
    return collection
}
export default {
    read
}