import Character from '../game/character/Character'
import Building, { BuildingID } from '../game/world/Building'
import BuildingTemplate from '../game/world/BuildingTemplate'
import Location, { LocationID } from '../game/world/Location'
import ServerGeography from '../game/world/ServerGeography'
import GlobalContext from '../globalContext/GlobalContext'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
interface BuildingTransfer {
    id: BuildingID,
    level: number,
}
interface ServerGeographyDataTransfer {
    serverId: string,
    discoveredLocations: Array<LocationID>,
    buildings: Array<BuildingTransfer>,

}  
const create = async (location: ServerGeography) => {
    const locations = Array.from( location.discoveredLocations.keys() )
    const buildings :Array<BuildingTransfer> = []
    location.buildings.forEach( building => {
        buildings.push({
            id: building.id,
            level: building.level,
        })
    })

    const transfer: ServerGeographyDataTransfer = {
        buildings: buildings,
        discoveredLocations: locations,
        serverId: location.serverId,
    }
    await redisClient.set('serverGeography:'+transfer.serverId,JsonUtils.stringify(transfer))
}
const read = async (serverID: string): Promise<ServerGeography | undefined> => {
    const serverLocation = await redisClient.get('serverGeography:'+serverID)
    if (serverLocation !== undefined && serverLocation !== null) {
        const transfer: ServerGeographyDataTransfer = JsonUtils.parse(serverLocation)
        const buildings = new Map<BuildingID, Building>()
        const locations = new Map<LocationID,Location>()
        transfer.buildings.forEach( building => {
            const buildingPrototype = GlobalContext.buildings.get(building.id) as BuildingTemplate
            if(buildingPrototype === undefined) {
                console.error("ERROR - LocationServerData.ts - buildingPrototype no existente")
            }
            const buildingData: Building = {
                id: buildingPrototype.id,
                name: buildingPrototype.name,
                description: buildingPrototype.description,
                level: building.level,
            }
            buildings.set(buildingData.id, buildingData)
        })
        transfer.discoveredLocations.forEach( locationID => {
            const locationPrototype = GlobalContext.locations.get(locationID) as Location
            if(locationPrototype === undefined) {
                console.error("ERROR - LocationServerData.ts - locationPrototype no existente")
            }
            locations.set(locationPrototype.id , locationPrototype)
        })
        return {
            buildings: buildings,
            discoveredLocations: locations,
            serverId: transfer.serverId,
        }
    }
    return undefined
}

export default { create, read }
