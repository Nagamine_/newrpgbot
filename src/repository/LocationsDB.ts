import { Collection } from 'discord.js';
import fs from 'fs'
import path from 'path'
import Location from '../game/world/Location'
import JsonUtils from '../utils/JsonUtils';
function read() {
    // Blocks the event loop
    const collection = new Collection<string,Location>()
    const locations = JsonUtils.parse(fs.readFileSync(path.join(__dirname,"../../simulatedDB/locations.json"), 'utf8'))
    locations.forEach( (element : Location) => {
        collection.set(element.id, element)
    });
    return collection
}
export default {
    read
}