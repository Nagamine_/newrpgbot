import Battle from '../game/battle/Battle'
import BattleContext from '../game/battle/BattleContext'
import BattleEntityUtils from '../game/battle/BattleEntityUtils'

//Lo ideal seria separar la logica de guardado en bd y local
//nota: no usar redis para guardar esto ya que ocurrira una condicion de carrera que impediará que se guarden los datos correctamente
//nota2: podriamos usar redis con un hack para que compartan los objetos y evitar la condicion de carrera
const save = async (battle: Battle) => {
    return BattleContext.battles.set(battle.id,battle)
}
const remove = async (battle_id: string ) => {
    const battle = await read(battle_id)
    battle?.entities.forEach(entity => {
        if(BattleEntityUtils.isHuman(entity)){
            deleteUserFromBattle(entity.user)
        }
    })
    BattleContext.battles.delete(battle_id)
}
const read = async (battle_id: string): Promise<Battle | undefined> => {
    return BattleContext.battles.get(battle_id)
}
const addUsertoBattle = async (battle_id: string, user_id: string) => {
    BattleContext.userIdToBattle.set(user_id,battle_id)
}
const deleteUserFromBattle = async (user_id: string) => {
    BattleContext.userIdToBattle.delete(user_id)
}
const getBattleIdFromUser = async (user_id: string) => {
    return BattleContext.userIdToBattle.get(user_id)
}
export default { save, read,remove,addUsertoBattle,getBattleIdFromUser,deleteUserFromBattle }
