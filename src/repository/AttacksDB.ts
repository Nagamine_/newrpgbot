import { Collection } from 'discord.js';
import fs from 'fs'
import path from 'path'
import Skill from '../game/skills/Skill';
import JsonUtils from '../utils/JsonUtils';
function read() {
    // Blocks the event loop
    const collection = new Collection<string,Skill>()
    const attacks = JsonUtils.parse(fs.readFileSync(path.join(__dirname,"../../simulatedDB/attacks.json"), 'utf8'))
    attacks.forEach( (element : Skill) => {
        collection.set(element.id, element)
    });
    return collection
}
export default {
    read
}