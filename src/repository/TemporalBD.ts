
import Temporal from '../game/character/Temporal'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
const create = async (temporal: Temporal,user_id: string) => {
    await redisClient.set('temporal:'+user_id,JsonUtils.stringify(temporal))
}
const read = async (user_id: string): Promise<Temporal | undefined> => {
    const userStr = await redisClient.get('temporal:'+user_id)
    if (userStr !== undefined && userStr !== null) {
        return JsonUtils.parse(userStr)
    }
    return undefined
}

export default { create, read }
