import Battle from '../game/battle/Battle'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
const save = async (battle: Battle) => {
    await redisClient.set('battle:'+battle.id,JsonUtils.stringify(battle.serialize()),{
       EX: 60*30
    })
}
const remove = async (battle_id: string ) => {
    await redisClient.del('battle:'+battle_id)
}
const read = async (battle_id: string): Promise<Battle | undefined> => {
    const battleStr = await redisClient.get('battle:'+battle_id)
    if (battleStr !== undefined && battleStr !== null) {
        return new Battle(JsonUtils.parse(battleStr))
    }
    return undefined
}
const addUsertoBattle = async (battle_id: string, user_id: string) => {
    await redisClient.set('battleUser:'+user_id,battle_id,{
        EX: 60*30
     })
}
const deleteUserFromBattle = async (user_id: string) => {
    await redisClient.del('battleUser:'+user_id)
}
const getBattleIdFromUser = async (user_id: string) => {
    return await redisClient.get('battleUser:'+user_id)
}
export default { save, read,remove,addUsertoBattle,getBattleIdFromUser,deleteUserFromBattle }
