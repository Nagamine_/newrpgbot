import { Collection } from 'discord.js';
import fs from 'fs'
import path from 'path'
import Archetipe from '../game/character/Archetipe';
import JsonUtils from '../utils/JsonUtils';
function read() {
    // Blocks the event loop
    const collection = new Collection<string,Archetipe>()
    const attacks = JsonUtils.parse(fs.readFileSync(path.join(__dirname,"../../simulatedDB/archetipes.json"), 'utf8'))
    attacks.forEach( (element : Archetipe) => {
        collection.set(element.name, element)
    });
    return collection
}
export default {
    read
}