import Character from '../game/character/Character'
import Inventory from '../game/character/Inventory'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
const create = async (inventory: Inventory) => {
    await redisClient.set('inventory:'+inventory.userId,JsonUtils.stringify(inventory))
}
const read = async (user_id: string): Promise<Inventory | undefined> => {
    const userStr = await redisClient.get('inventory:'+user_id)
    if (userStr !== undefined && userStr !== null) {
        return  JsonUtils.parse(userStr)
    }
    return undefined
}

export default { create, read }
