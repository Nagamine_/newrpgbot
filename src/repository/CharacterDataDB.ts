import CharacterData from '../game/character/CharacterData'
import JsonUtils from '../utils/JsonUtils'
import redisClient from './RedisConfig'
const create = async (CharacterData: CharacterData) => {
    await redisClient.set('characterData:'+CharacterData.userId ,JsonUtils.stringify(CharacterData))
}
const read = async (user_id: string): Promise<CharacterData | undefined> => {
    const userStr = await redisClient.get('characterData:'+user_id)
    if (userStr !== undefined && userStr !== null) {
        return  JsonUtils.parse(userStr)
    }
    return undefined
}

export default { create, read }
