//config dotenv
import dotenv from 'dotenv'
dotenv.config()
// Require the necessary discord.js classes
import { Client, GatewayIntentBits, Partials } from 'discord.js'
import { loadCommands, loadInteractions } from './discord/loader'
import { ejecutarComando, ejecutarInteraccion } from './discord/execute'
import globalContext from './globalContext/GlobalContext'
import AttacksDB from './repository/AttacksDB'
import ArchetipesDB from './repository/ArchetipesDB'
import { loadSkillScripts } from './game/skillScripts/SkillScriptsRouter'
import redisClient from './repository/RedisConfig'
import LocationsDB from './repository/LocationsDB'
import BuildingTemplateDB from './repository/BuildingTemplateDB'
// Create a new client instance
const client = new Client({
    intents: [
        /*
		Intents 'GUILDS' is required
		if you wish to receive (message) events
		from guilds as well.

		If you don't want that, do not add it.
		Your bot will only receive events
		from Direct Messages only.
	*/
        GatewayIntentBits.Guilds,
        GatewayIntentBits.DirectMessages,
        GatewayIntentBits.MessageContent,
        GatewayIntentBits.GuildMessages,
    ],
    partials: [Partials.Message, Partials.Channel], // Needed to get messages from DM's as well
})
globalContext.commands = loadCommands()
globalContext.interactionCommands = loadInteractions()
globalContext.skills = AttacksDB.read()
globalContext.skillScripts = loadSkillScripts()
globalContext.archetipes = ArchetipesDB.read()
globalContext.locations = LocationsDB.read()
globalContext.buildings = BuildingTemplateDB.read()
console.log("finish loading")
redisClient.on('error', function (err) {
    console.log(err.message)
})
redisClient.on('connect', () => console.log('Redis conectado'))
redisClient.on('ready', () => console.log('Redis Listo'))
redisClient.on('reconnecting', () => console.log('reconnecting'))
redisClient.on('end', () => console.log('end'))
;(async () => {
    await redisClient.connect()
})()

// When the client is ready, run this code (only once)
client.once('ready', () => {
    console.log('Ready!')
})
client.on('messageCreate', async (msg) => {
    // This block will prevent the bot from responding to itself and other bots
    if (msg.author.bot) {
        return
    }
    ejecutarComando(msg)
})
client.on('interactionCreate', async (interaction) => {
    ejecutarInteraccion(interaction)
})
// Login to Discord with your client's token
client.login(process.env.DISCORD_TOKEN)
