//Archivo usaddo para modificar la redis
import dotenv from 'dotenv'
dotenv.config()
//dotenvb siempre antes de los imports
import CharacterDataDB from './repository/CharacterDataDB';
import InventoryDB from "./repository/InventoryDB";
import BattleBD from './repository/BattleBD';

import redisClient from './repository/RedisConfig'
redisClient.on('error', function (err) {
    console.log(err.message)
})
redisClient.on('connect', () => console.log('Redis conectado'))
redisClient.on('ready', () => console.log('Redis Listo'))
redisClient.on('reconnecting', () => console.log('reconnecting'))
redisClient.on('end', () => console.log('end'))
;(async () => {
    await redisClient.connect()

    // const inventory = {
    //     userId: "612822306556870656",
    //     items:new Map<string, number>()
    // }
    // inventory.items.set("madera", 6);
    // await InventoryDB.create(inventory, "612822306556870656");
    // const char = await CharacterDataDB.read("612822306556870656")
    // char!.alive = true
    // await CharacterDataDB.create(char!)
    await BattleBD.remove('612822306556870656')
    console.log("OK")
    await redisClient.disconnect()
    return
})()
