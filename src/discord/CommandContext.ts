import Battle from "../game/battle/Battle"
import Character from "../game/character/Character"
import CharacterData from "../game/character/CharacterData"
import Inventory from "../game/character/Inventory"
import Location from "../game/world/Location"
import ServerGeography from "../game/world/ServerGeography"

export default interface CommandContext {
    character?: Character
    inventory?: Inventory
    characterData?: CharacterData
    characterLocation? : Location
    battle?: Battle
    serverGeography?: ServerGeography
    args: string[]
}
export interface CommandContextCharacter extends CommandContext{
    character: Character
   }