export interface CommandCategory{
    name: string,
    description: string,
    order: number,
    hidden?: boolean
} 


export const CategoryList = {
    START: {
        name: "start",
        description: "Comandos para empezar a jugar",
        order: 0
    },
    INFORMATION: {
        name: "information",
        description: "Comandos para obtener información",
        order: 1
    },
    GAMEPLAY: {
        name: "gameplay",
        description: "Comandos del core del gameplay",
        order: 2
    },
    CHARACTER: {
        name: "character",
        description: "Comandos para gestionar tu personaje",
        order: 2
    },
    BATTLE: {
        name: "battle",
        description: "Comandos para luchar",
        order: 3
    },
    UTILITY: {
        name: "utility",
        description: "Comandos de utilidad",
        order: 4
    },
    TEST: {
        name: "test",
        description: "Comandos de prueba",
        order: 5,
    }
}