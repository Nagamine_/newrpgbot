import CommandContext from "./CommandContext"
export interface CharacterPermisos {
    basicInfo?: boolean 
    inventory?: boolean
    data?: boolean | {
        alive?: boolean
    }
    battle?: boolean //true: solo batalla, false: no se puede usar en batalla, undefined: no importa
}
export interface LocationPermisos {
    requiredBuildings?: Array<{
        buildingId: string,
        level?: number
    }>
}
export default interface ComandoPermisos {
    character?: CharacterPermisos
    location?: boolean | LocationPermisos
    servidor?: 'ninguno' | 'cualquiera' | 'registrado'
    cooldown?: number //en segundos
    conditional?: {fn:(ctx:CommandContext)=>boolean, message: string, permisions: ComandoPermisos}
    argNumber?: number
}