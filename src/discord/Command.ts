import { Message } from 'discord.js'
import CommandPermisions from './CommandPermisions'
import CommandContext from './CommandContext'
import { CommandCategory } from './CommandCategory'
import { Interactable } from '../game/battle/BattleInteractionManager'
export interface Command{
    name: string,
    alias: Array<string>,
    description: string,
    permisions?: CommandPermisions,
    category: CommandCategory,
    execute: (msg: Message, ctx: Required<CommandContext>) => void
}
export interface InteractionCommand<Interact extends Interactable>{
    name: string,
    description: string,
    permisions?: CommandPermisions,
    execute: (interaction: Interact, ctx: Required<CommandContext>) => void
}