import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from "discord.js"
import BattleEntity from "../../../game/battle/BattleEntity"

export default function createObjetivesButtons(objetives: Array<BattleEntity>) {
    const objectiveArray = Array<ButtonBuilder>()
    objetives.forEach((objetivo) => {
        objectiveArray.push( new ButtonBuilder().setLabel(objetivo.name).setCustomId(`objectiveButton::${objetivo.id.toString()}`).setStyle(ButtonStyle.Primary))
    }
    )
    const row = new ActionRowBuilder<ButtonBuilder>().addComponents(
        objectiveArray
    )
    return {
        content: `elije un objetivo`,
        components: [row],
        ephemeral: true,
    }
}