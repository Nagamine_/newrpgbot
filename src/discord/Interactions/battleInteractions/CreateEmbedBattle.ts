import { APIEmbedField, EmbedBuilder } from "discord.js"
import Battle from "../../../game/battle/Battle"

export default function createEmbedBattle(battle: Battle) {
    //copy the entities array
    const entities = battle.entities.slice()
    //create the embed fields from the entities array sorting by team
    const embedFields: Array<APIEmbedField> = []
    entities.sort((a, b) => a.bando.localeCompare(b.bando))
    let currentBando = ""
    entities.forEach(entity => {
        if (entity.bando !== currentBando) {
            currentBando = entity.bando
            embedFields.push({
                name: "\u200b",
                value: "**Bando " + currentBando+"**",
            })
        }
        embedFields.push({
            name: entity.name,
            value: `Vida\n${"`"}${entity.currentStats.vida}/${entity.stats.vida}${"`"}\nVelocidad\n${"`"}${entity.currentStats.velocidad}${"`"}`,
            inline: true
        })
    })
    let orderedTurnsString = battle.orderedTurns.reverse().map(entity => entity.name + " (" + entity.turn.nextTurn + ")").join(" << ")
    if(orderedTurnsString.length === 0){
        orderedTurnsString = "construyendo turnos..."
    }
    embedFields.push({
        name: "Turnos",
        value: orderedTurnsString
    })
    const embed = new EmbedBuilder()
        .setColor(0xB3001B)
        .setTitle('Battle')
        .setFields(embedFields)
        .setFooter({ text: battle.id.toString() })

    return {
        embeds: [embed]
    }
}