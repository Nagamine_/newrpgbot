import { ActionRowBuilder,  ButtonStyle, ButtonBuilder } from "discord.js"
import BattleEntity from "../../../game/battle/BattleEntity"

export default function createSkillButtons(userBattleEntity: BattleEntity) {
    const skills = userBattleEntity.skills
    if (skills.length === 0) {
        return {
            content: `No tienes ataques disponibles`,
            ephemeral: true,
        }
    }
    const skillArray = Array<ButtonBuilder>()
    skills.forEach((skill) => {
        const cooldown = userBattleEntity.turn.cooldowns[skill.id]
        const button = new ButtonBuilder()
            .setCustomId(`skill::${skill.id}`)
            .setLabel(`${skill.name} (${cooldown})`)
            .setStyle(ButtonStyle.Primary)
            .setDisabled(cooldown > 0)
        skillArray.push(button)
    })
    const row = new ActionRowBuilder<ButtonBuilder>()
        .addComponents(skillArray)

    return {
        content: `${userBattleEntity.name} es tu turno!\nElije una habilidad`,
        components: [row],
        ephemeral: true,
    }
}