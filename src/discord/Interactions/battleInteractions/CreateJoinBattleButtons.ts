import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from "discord.js"
import Battle from "../../../game/battle/Battle"

export default function createJoinBattleButtons(battle: Battle) {
    const row = new ActionRowBuilder<ButtonBuilder>()
    .addComponents(
        new ButtonBuilder()
            .setCustomId(`joinBattle::${battle.id}`)
            .setLabel("Unirse")
            .setStyle(ButtonStyle.Primary),
        new ButtonBuilder()
        .setCustomId(`startBattle::${battle.id}`)
            .setLabel("Iniciar")
            .setStyle(ButtonStyle.Secondary)
    )   

    return {
        content: `La batalla va a iniciar, en espera:`,
        components: [row],
    }
}