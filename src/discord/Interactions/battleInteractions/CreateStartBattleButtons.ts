import { ActionRowBuilder, ButtonBuilder, ButtonStyle } from "discord.js";
import Battle from "../../../game/battle/Battle";
import createEmbedBattle from "./CreateEmbedBattle";

export default function createStartBattleButtons(battle : Battle, msg: string = `Batalla iniciada` ) {
    const row = new ActionRowBuilder<ButtonBuilder>()
    .addComponents(
        new ButtonBuilder()
            .setCustomId(`startBattle`)
            .setLabel("Atacar")
            .setStyle(ButtonStyle.Primary)
    );
    const embedBattle = createEmbedBattle(battle)
    return {
        ...embedBattle,
        content: msg,
        components: [row],
    }
}