import { itemRewardToString } from "../../../game/items/ItemUtils"
import Location from "../../../game/world/Location"

export default function createLocationInfoEmbed(location: Location) {
    const enemys = location?.enemies.map(enemy => enemy.name /*+ " ["+itemRewardToString(enemy.items)+"]"*/).join('\n')
    const items: string= itemRewardToString(location.rewardItems)
    const embed =  {
        "title": "Información del mundo",
        "description": `Estas en:  \`${location.name}\``,
        "color": 0x00FFFF,
        "fields": [
          {
            "name": `Salario`,
            "value": location.salary.toLocaleString(),
            "inline": true
          },
          {
            "name": `Enemigos`,
            "value": enemys,
            "inline": true
          },
          {
            "name": `Exploración`,
            "value": items,
            "inline": true
          }
        ]
      }

    return {embeds: [embed]}
}