import { StringSelectMenuBuilder } from "discord.js";
import GlobalContext from "../../../globalContext/GlobalContext";

export default function createArchetipesSelect() {
    let archetipes = ''
    const values: { label: string; value: string; default: boolean }[] = []
    GlobalContext.archetipes.forEach((archetipe) => {
        archetipes += `\n\n> **${archetipe.name}**\n`+
        `> ${archetipe.description}`
        values.push({
            label: archetipe.name,
            value: archetipe.name,
            default: false,
        })
    })
    return {
        embeds: [
            {
                title: `Bienvenido a Thasmel`,
                description: "Este un un rpg ambientado en un mundo magitech o magicpunk donde la fantasia y la magia se mezcla con la tecnologia.\n\n"+
                "**Escoje tu equipo inicial**\n"+
                `En este juego tus stats y tus ataques se determinan por los items que tengas equipados asi que puedes cambiar siempre que quieras${archetipes}`,
                color: 0xd013ff,
            },
        ],
        components: [
            {
                type: 1,
                components: [
                    new StringSelectMenuBuilder()
                    .addOptions(values)
                    .setCustomId("archetipeSelect")
                    .setPlaceholder("Selecciona tu arquetipo"),
                ],
            },
        ],
    }

}