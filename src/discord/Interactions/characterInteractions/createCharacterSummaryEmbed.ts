import Character from "../../../game/character/Character";
import { characterToBattleEntity, profileStatsToString } from "../../../game/character/CharacterUtils";
import CharacterData from "../../../game/character/CharacterData";
import Inventory from "../../../game/character/Inventory";
import createSelectProfileDropdown, { SelectProfileDefault } from "./createSelectProfileDropdown";

//create an embed with the character information
export default function createCharacterSummaryEmbed(character: Character,characterData:CharacterData, inventory: Inventory) {
    const battleEntity = characterToBattleEntity(character,characterData ,"")

    const embed = {
        title: "Resumen de personaje",
        description: `${character.name}`,
        color: 0x0099ff,
        fields: [
            {
                name: "Oro",
                value: inventory.items.get("oro")+"",
                inline: false
            },
            {
                name: "Stats",
                value: "```"+ profileStatsToString(battleEntity.stats,battleEntity.currentStats) +"```",
                inline: true
            },
            {
                name: "Equipo",
                value:  "```"+Array.from(character.equipment.values()).map( item => item.name).join("\n")+"```",
                inline: true
            },
            {
                name: "Habilidades",
                value: "```"+ (battleEntity.skills.length > 0 ? character.skills.join(", ") : "No hay habilidades disponibles")+"```",
                inline: false
            },
        ],
    };
    return {
        embeds: [
           embed
        ],
        components: [createSelectProfileDropdown(SelectProfileDefault.RESUMEN)]
    };
}