import { EmbedBuilder } from "discord.js"
import Character from "../../../game/character/Character"
import { statsToString } from "../../../game/character/CharacterUtils";
import Inventory from "../../../game/character/Inventory";
import createSelectProfileDropdown, { SelectProfileDefault } from "./createSelectProfileDropdown";

//export a function that creates an embed with the character's equipment
export default function createCharacterInventoryEmbed(inventory: Inventory) {
    let inventoryEmbed: { name: string; value: string; inline: boolean }[] = []
    let inventoryString = "```"
    inventory.items.forEach((item,key) => {
        inventoryString += "\n"
        inventoryString += key + ": " + item 
    })
    inventoryString += "```\n"
    inventoryEmbed.push({
        name: "Inventario",
        value: inventoryString,
        inline: false,
    })
    const embed = new EmbedBuilder()
        .setColor('#0099ff')
        .setTitle('Inventario')
        .setDescription('Inventario del personaje')
        .addFields(
            inventoryEmbed
        )
    return  {
        embeds: [
           embed
        ],
        components: [createSelectProfileDropdown(SelectProfileDefault.INVENTARIO)]
    };
}