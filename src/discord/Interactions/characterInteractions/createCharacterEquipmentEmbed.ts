import { EmbedBuilder } from "discord.js"
import Character from "../../../game/character/Character"
import { statsToString } from "../../../game/character/CharacterUtils";
import createSelectProfileDropdown, { SelectProfileDefault } from "./createSelectProfileDropdown";

//export a function that creates an embed with the character's equipment
export default function createCharacterEquipmentEmbed(character: Character) {
    let equipmentEmbed: { name: string; value: string; inline: boolean }[] = []
    character.equipment.forEach((item) => {
        let equipmentString = ''
        equipmentString += "\n"
        + "Pieza\n"
        + "```" + item.bodyPart + "```\n"
        + "Stats\n"
        + "```" + statsToString(item.stats) + "```\n"
      
        equipmentEmbed.push({
            name: item.name,
            value: equipmentString,
            inline: true,
        })
    })
    const embed = new EmbedBuilder()
        .setColor('#0099ff')
        .setTitle('Equipo')
        .setDescription('Equipo del personaje')
        .addFields(
            equipmentEmbed
        )
    return  {
        embeds: [
           embed
        ],
        components: [createSelectProfileDropdown(SelectProfileDefault.EQUIPO)]
    };
}