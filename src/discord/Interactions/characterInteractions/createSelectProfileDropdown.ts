import { ActionRowBuilder,StringSelectMenuBuilder, StringSelectMenuOptionBuilder } from "discord.js";

//export a funciton that creates a select menu with the options resumen, equipo, ataques, inventario
export enum SelectProfileDefault{
    RESUMEN = "resumen",
    EQUIPO = "equipo",
    HABILIDADES = "habilidades",
    INVENTARIO = "inventario"
}
export default function createSelectProfileDropdown(selectDefault?: SelectProfileDefault) {
    const select = new ActionRowBuilder<StringSelectMenuBuilder>()
        .addComponents(
            new StringSelectMenuBuilder()
                .setCustomId('selectProfile')
                .setPlaceholder('Selecciona una opción')
                .addOptions(
                    new StringSelectMenuOptionBuilder()
                    .setLabel('Resumen')
                    .setValue('resumen')
                    .setDescription('Muestra un resumen de tu personaje')
                    .setDefault(selectDefault === SelectProfileDefault.RESUMEN),

                    //Transform this into StringSelectMenuOptionBuilder
                    new StringSelectMenuOptionBuilder()
                    .setLabel('Equipo')
                    .setValue('equipo')
                    .setDescription('Muestra el equipo que llevas equipado')
                    .setDefault(selectDefault === SelectProfileDefault.EQUIPO),
                    new StringSelectMenuOptionBuilder()
                    .setLabel('Habilidades')
                    .setValue('habilidades')
                    .setDescription('Muestra las habilidades que puedes realizar')
                    .setDefault(selectDefault === SelectProfileDefault.HABILIDADES),
                    new StringSelectMenuOptionBuilder()
                    .setLabel('Inventario')
                    .setValue('inventario')
                    .setDescription('Muestra el inventario de tu personaje')
                    .setDefault(selectDefault === SelectProfileDefault.INVENTARIO)
                    ),
        );
    return select;
}