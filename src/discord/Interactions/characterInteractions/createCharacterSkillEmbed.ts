import { EmbedBuilder } from "discord.js"
import Character from "../../../game/character/Character"
import { modifiersToString } from "../../../game/character/CharacterUtils";
import GlobalContext from "../../../globalContext/GlobalContext";
import createSelectProfileDropdown, { SelectProfileDefault } from "./createSelectProfileDropdown";

//export a function that creates an embed with the character's equipment
export default function createCharacterSkillEmbed(character: Character) {
    let skillEmbed: { name: string; value: string; inline: boolean }[] = []
    character.skills.forEach((skillKey) => {
        const skill = GlobalContext.skills.get(skillKey)
        if(skill === undefined){
            console.error("Error - skill not found "+ skillKey)
            return
        }
        let skillString = ''
        skillString += "\n"
        +skill.description + "\n"	
        +"```"+modifiersToString(skill.modifier)+"```"
        + "Objetivo\n"
        + "```" + skill.target + "```\n"

        skillEmbed.push({
            name: skill.name,
            value: skillString,
            inline: true,
        })
    })
    const embed = new EmbedBuilder()
        .setColor('#0099ff')
        .setTitle('Habilidades')
        .setDescription('Habilidades del personaje')
        .addFields(
            skillEmbed
        )
    return  {
        embeds: [
           embed
        ],
        components: [createSelectProfileDropdown(SelectProfileDefault.HABILIDADES)]
    };
}