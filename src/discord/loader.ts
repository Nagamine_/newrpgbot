import { ButtonInteraction, Collection, StringSelectMenuInteraction } from "discord.js";
import path from "path";
import fs from "fs"
import { Command, InteractionCommand } from "./Command";
let walk = function(dir:string) {
    let results: Array<string> = [];
    let list = fs.readdirSync(dir);
    list.forEach(function(file) {
        file = dir + '/' + file;
        let stat = fs.statSync(file);
        if (stat && stat.isDirectory()) { 
            /* Recurse into a subdirectory */
            results = results.concat(walk(file));
        } else { 
            /* Is a file */
            results.push(file);
        }
    });
    return results;
}
export function loadCommands(){
    let commands = new Collection<string,Command>();
    const commandsPath = path.join(__dirname,".." ,'commands/textCommands');
    const commandFiles = walk(commandsPath).filter(file => file.endsWith('.ts') || file.endsWith('.js'));
    for (const file of commandFiles) {
        const command = import(file);
   
        command.then((it)=>{
            let comando:Command = it.default
            commands.set(comando.name?.toLocaleLowerCase(),comando)
            comando.alias?.forEach(al => {
                commands.set(al.toLocaleLowerCase(),comando)
            })
        })
        // Set a new item in the Collection
        // With the key as the command name and the value as the exported module
        //client.commands.set(command.data.name, command);
    }
    return commands
}

export function loadInteractions(){
    let commands = new Collection<string,InteractionCommand<ButtonInteraction | StringSelectMenuInteraction>>();
    const commandsPath = path.join(__dirname,".." ,'commands/interactionCommands');
    const commandFiles = walk(commandsPath).filter(file => file.endsWith('.ts') || file.endsWith('.js'));
    
    for (const file of commandFiles) {
        const command = import(file);
        command.then((it)=>{
            let comando:InteractionCommand<ButtonInteraction | StringSelectMenuInteraction> = it.default
            commands.set(comando.name?.toLocaleLowerCase(),comando)
        })
        // Set a new item in the Collection
        // With the key as the command name and the value as the exported module
        //client.commands.set(command.data.name, command);
    }
    console.log("loaded interactions")
    return commands
}
