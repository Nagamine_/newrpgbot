import { APIEmbed, ActionRowBuilder, Embed, Interaction, Message, StringSelectMenuBuilder, User } from "discord.js"
import globalContext from "../globalContext/GlobalContext"
import BattleBD from "../repository/BattleBD"
import CharacterDataDB from "../repository/CharacterDataDB"
import CharactersDB from "../repository/CharactersDB"
import InventoryDB from "../repository/InventoryDB"
import TemporalBD from "../repository/TemporalBD"
import CommandContext from "./CommandContext"
import ComandoPermisos from "./CommandPermisions"
import { createCharacter } from "../game/character/Character"
import createCharacterSummaryEmbed from "./Interactions/characterInteractions/createCharacterSummaryEmbed"
import ServerGeographyDB from "../repository/ServerGeographyDB."
import { createServerGeography } from "../game/world/ServerGeography"
import { getCurrentLocation } from "../game/character/CharacterData"
import GlobalContext from "../globalContext/GlobalContext"
type commandFilter = (command: string, args: string[], id: User, serverId: string | null, reply: (msg: string, embeds?: APIEmbed[], components?: ActionRowBuilder<StringSelectMenuBuilder>[]) => void, permisions?: ComandoPermisos, context?: CommandContext) => Promise<CommandContext | null>


const filterCommands: commandFilter = async (command: string, args: string[], user: User, serverId: string | null, reply: (msg: string, embeds?: APIEmbed[], components?: ActionRowBuilder<StringSelectMenuBuilder>[]) => void, permisions?: ComandoPermisos, ctx?: CommandContext): Promise<CommandContext | null> => {
    let context: CommandContext = ctx ?? {
        battle: undefined,
        character: undefined,
        serverGeography: undefined,
        args: []
    }
    if (permisions === undefined) {
        return context
    }
    if (permisions.servidor !== "ninguno" && serverId === null) {
        reply("Este comando solo se puede usar en un servidor")
        return context
    }
    const locationPerm = permisions.location
    if (locationPerm || permisions?.character?.data !== undefined) {
        let geography = await ServerGeographyDB.read(serverId!)
        if (geography === undefined) {
            geography = createServerGeography(serverId!)
            await ServerGeographyDB.create(geography)
        }
        context.serverGeography = geography
        if(typeof locationPerm !== "boolean" && locationPerm !== undefined){
            //use a regular foreach
            if(locationPerm.requiredBuildings !== undefined){
                for(const building of locationPerm.requiredBuildings){  
                    const selectedBuilding = geography?.buildings.get(building.buildingId)
                    if(selectedBuilding === undefined){
                        reply(`Necesitas construir ${GlobalContext.buildings.get(building.buildingId)?.name} para usar este comando`)
                        return null
                    }
                    if(building.level !== undefined && selectedBuilding.level < building.level){
                        reply(`Necesitas tener ${GlobalContext.buildings.get(building.buildingId)?.name} en nivel ${building.level} para usar este comando`)
                        return null
                    }
                }
            }
        }
    }
    if (permisions.character !== undefined) {
        let character = await CharactersDB.read(user.id)
        if (character === undefined) {
            const char = await createCharacter(user.username, user.id)
            character = char.character
            const charReply = createCharacterSummaryEmbed(char.character, char.characterData, char.inventory)
            reply("Bienvenido al juego, este es tu nuevo personaje", charReply.embeds, charReply.components)
        }
        context.character = character

        if (permisions.character.inventory === true) {
            let inventory = await InventoryDB.read(user.id)
            if (inventory === undefined) {
                reply("No tienes un personaje creado")
                return null
            }
            context.inventory = inventory
        }
        if (permisions.character.data !== undefined) {
            let data = await CharacterDataDB.read(user.id)
            if (data === undefined) {
                reply("No tienes un personaje creado")
                return null
            }
            context.characterData = data
            if (typeof permisions.character.data === "object") {
                if (permisions.character.data.alive === true) {
                    if (context.characterData.alive === false) {
                        reply("No puedes usar este comando estando muerto, usa `Heal` para revivir")
                        return null
                    }
                }
            }
            const location = getCurrentLocation(context.characterData, serverId)
            const discoveredLocations = context.serverGeography?.discoveredLocations!.get(location)
            if (discoveredLocations === undefined) {
                reply("Error "+location + " no esta descubierta en este server")
                console.error("Error - execute.ts - filterCommands " + location + " no esta descubierta en este server")
                return null
            }
            context.characterLocation = discoveredLocations
        }
        if (permisions.character.battle === true) {
            const battleId = await BattleBD.getBattleIdFromUser(user.id)
            if (battleId === undefined || battleId === null) {
                reply("No estas en un combate")
                return null
            }
            const battle = await BattleBD.read(battleId)
            if (battle === undefined) {
                reply("La batalla no existe")
                return null
            }
            context.battle = battle

        } else if (permisions.character.battle === false) {
            const battleId = await BattleBD.getBattleIdFromUser(user.id)
            if (battleId) {
                reply("No puedes usar este comando en un combate")
                return null
            }
        }

    }
    if (permisions?.argNumber !== undefined && permisions?.argNumber > 0) {
        if (args.length < permisions?.argNumber) {
            reply(`Faltan argumentos`)
            return null
        }
    }
    if (permisions?.cooldown) {
        let newTemp = await TemporalBD.read(user.id)
        const waitTimeMs = permisions?.cooldown * 1000
        if (newTemp) {
            if (newTemp[command] && Date.now() - newTemp[command] < waitTimeMs) {
                const remainingTime = Math.floor((waitTimeMs - (Date.now() - newTemp[command])))
                //show message to wait in reaminig hours and minutes, example 1h 30m
                if (remainingTime >= (1000 * 60 * 60)) {
                    const hours = Math.floor(remainingTime / (1000 * 60 * 60))
                    const minutes = ((remainingTime / (1000 * 60)) % 60) + 1
                    reply(`Debes esperar ${hours} ${hours > 1 ? 'horas' : 'hora'} y ${minutes} ${minutes !== 1 ? 'minutos' : 'minuto'} para volver a usar este comando`)
                } else if (remainingTime >= (1000 * 60)) {
                    const minutes = Math.floor(remainingTime / (1000 * 60))
                    const segundos = Math.floor((remainingTime / 1000) % 60)
                    reply(`Debes esperar ${minutes} ${minutes > 1 ? 'minutos' : 'minuto'} y ${segundos} ${segundos !== 1 ? 'segundos' : 'segundo'} para volver a usar este comando`)
                } else {
                    const segundos = Math.floor(remainingTime / 1000)
                    reply(`Debes esperar ${segundos} ${segundos > 1 ? 'segundos' : 'segundo'} para volver a usar este comando`)
                }
                return null
            } else {
                newTemp[command] = Date.now()
                TemporalBD.create(newTemp, user.id)
            }
        } else {
            newTemp = {
                [command]: Date.now()
            }
            TemporalBD.create(newTemp, user.id)
        }
    }
    //Ejecuta los permisos solo si se cumple fn
    if (permisions?.conditional) {
        const conditional = permisions?.conditional
        if (conditional.fn(context)) {
            let conditionalCtx = await filterCommands(command, args, user, serverId, reply, conditional.permisions, context)
            if (conditionalCtx) {
                context = conditionalCtx
            } else {
                return null
            }
        } else {
            reply(conditional.message)
            return null
        }
    }
    return context
}


export async function ejecutarComando(msg: Message) {
    let prefix = "!"
    let content = msg.content
    let comandos = globalContext.commands
    if (!content.startsWith(prefix)) {
        return
    }
    let body = content.slice(prefix.length)
    const splitBody = body.split(/ +/)
    if (splitBody.length === 0) {
        return
    }
    let command = splitBody[0].toLocaleLowerCase()
    if (!comandos.has(command)) {
        return
    }
    const replyInteraction = (str: string, embeds?: APIEmbed[], components?: ActionRowBuilder<StringSelectMenuBuilder>[]) => {
        msg.reply({
            content: str,
            embeds: embeds,
            components: components
        })
    }
    const executable = comandos.get(command)
    const args = splitBody.slice(1, splitBody.length)
    const context = await filterCommands(command, args, msg.author, msg.guildId, replyInteraction, executable?.permisions)
    if (context) {
        context.args = args
        executable?.execute(msg, context as Required<CommandContext>)
    }
}

export async function ejecutarInteraccion(interaction: Interaction) {
    let comandos = globalContext.interactionCommands
    if (!((interaction.isStringSelectMenu() || interaction.isButton()) && interaction.isRepliable())) {
        return
    }
    const splitedArgs = interaction.customId.split("::")
    let command = splitedArgs[0].toLocaleLowerCase()
    if (!comandos.has(command)) {
        interaction.reply({
            content: `Error, no existe el comando`
        })
        console.error("Error - execute.ts - comando " + command + " no existe")
        return
    }

    const executable = comandos.get(command)
    const replyInteraction = (msg: string, embeds?: APIEmbed[], components?: ActionRowBuilder<StringSelectMenuBuilder>[]) => {
        interaction.reply({
            content: msg,
            embeds: embeds,
            components: components
        })
    }
    const args = splitedArgs.slice(1, splitedArgs.length)
    if(interaction.isStringSelectMenu()){
        args.push(...interaction.values)
    }
    const context = await filterCommands(command, args, interaction.user, interaction.guildId, replyInteraction, executable?.permisions)
    if (!context) {
        return
    }
    context.args = args
    executable?.execute(interaction, context as Required<CommandContext>)


}



