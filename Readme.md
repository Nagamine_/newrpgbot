# BestoRPG
## Run command
```
npm run dev
```
or
```
npm run start
```
## Documentacion para el futuro
- Los comandos estan en la carpeta commands, se registran automaticamente leyendo los archivos
- Se guardan los datos cargados de la bd y de los archivos en globalcontext, incluyendo los comandos
- Se intento seguir ubna arquitectura orientada a datos
- Se usa redis para la base de datos, mientras que se simula una con json
- Los archivos de juego estan en la carpeta game
- Se deserializa usando JsonUtils, ya que permite serializar los map
- La logica de combate se desarrolla principalmente en BattleInteractionManager, teniendo como auxiliar BattleManager para logica mas especifica

## To do
 - Refactor battleUtils
 - Revisar si se debe refactorizar BattleEntity para simplificar su uso